var gulp = require('gulp');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var watchify = require('watchify');
var reactify = require('reactify');
var babelify = require('babelify');
var notify = require("gulp-notify");
var livereload = require('gulp-livereload');
var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var nodePath = require('path');
var livereactload = require('livereactload');
var CopyWebpackPlugin = require('copy-webpack-plugin');

var path = {
	HTML: 'src/main.html',
	MINIFIED_OUT: 'build.min.js',
	mainJS: './src/js/Main.js',
	DEST: 'dist',
	DEST_BUILD: 'dist/build',
	DEST_SRC: 'dist/src',
	OUT: 'build.js'
}

gulp.task('copy', function() {
	gulp.src(path.HTML)
		.pipe(gulp.dest(path.DEST));
});

gulp.task('browserify', function() {
	gulp.watch(path.HTML, ['copy']);

    //var livereactload2 = livereactload({server: false});
    var bundler = browserify({
        entries: [path.mainJS], // Only need initial file, browserify finds the deps
        transform: [babelify], // We want to convert JSX to normal javascript
        debug: true, // Gives us sourcemapping
        plugin: [livereactload],
        cache: {}, packageCache: {}, fullPaths: true // Requirement of watchify
    });
    var watcher  = watchify(bundler);
    //livereload.listen({start: true, reloadPage: 'http://0.0.0.0:8000/dist/main.html'});

    return watcher
    .on('update', function () { // When any files update
        var updateStart = Date.now();
        console.log('Updating!');
        watcher.bundle() // Create new bundle that uses the cache for high performance
        .on('error', notify.onError({
            message: 'Error: <%= error.message %>'
        }))
        .pipe(source(path.OUT))
        // This is where you add uglifying etc.
        .pipe(gulp.dest(path.DEST_SRC))
        //.pipe(livereload({start: true}));
        console.log('Updated!', (Date.now() - updateStart) + 'ms');

    })
    .bundle() // Create the initial bundle when starting the task
    .pipe(source(path.OUT))
    .pipe(gulp.dest(path.DEST_SRC))
    //.pipe(livereload({start: true}));
});

gulp.task('webpack-dev-server', function(callback) {
    var compiler = webpack({
        devtool: 'eval',

        entry: [
            'webpack-dev-server/client?http://localhost:4000',
            'webpack/hot/only-dev-server',
            './src/js/Main'
        ],

        output: {
            path: nodePath.join(__dirname, 'dist'),
            filename: 'bundle.js',
            publicPath: '/dist/'
        },

        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            // new CopyWebpackPlugin([
            //     { from: 'statics/dog.jpgafsdasdfads' ,
            //      to: 'hello.jpg'}
            // ])
        ],

        module: {
            loaders: [
                {
                    loaders: ['react-hot', 'babel-loader?presets[]=es2015,presets[]=react'],
                    include: nodePath.join(__dirname, 'src'),
                    exclude: ['/node_modules/' , '/my_node_modules/', '/dist/']
                },

                {
                    test: /\.(png|jpg|jpeg|svg)$/, 
                    loader: 'file-loader' ,
                    exclude: ['/node_modules/' , '/my_node_modules/', '/dist/']
                },

                {
                    test: /\.css$/, 
                    loader: "style-loader!css-loader" ,
                    exclude: ['/node_modules/' , '/my_node_modules/', '/dist/']
                }
           ]
        }
    });

    new WebpackDevServer(compiler, {
        publicPath: '/dist/',
        hot: true, 
        historyApiFallback: true,
        progress: true,
        displayErrorDetails: true
    }).listen(4000, 'localhost', function (err, result) {
        if (err) {
            return console.log(err);
        }
        console.log('Listening at http://localhost:4000/');
    });

});

gulp.task('default', ['browserify']);