var React = require('react');
var ReactDOM = require('react-dom');
var ReactDnD = require('react-dnd');
var ReactDnDHTMLBackend = require('react-dnd-html5-backend');
var PureRenderMixin = require('react-addons-pure-render-mixin');
var Immutable = require('immutable');
var Radium = require('radium');
const {Style, StyleRoot} = Radium;

var Perf = require('react-addons-perf');

var FormDesignerActions = require('./actions/FormDesignerActions.js');
var FormEntityStore = require('./stores/FormEntityStore.js');
var AppStateStore = require('./stores/AppStateStore.jsx');
var HelperFunctions = require('./HelperFunctions.jsx');

var Constants = require('./constants/Constants.js');
const FocusOptions = Constants.FOCUS_OPTIONS;

var DragHome = require('./components/DragHome.jsx').DraggableElementsContainer;
var FormLayout = require('./components/FormLayout.jsx').FormLayout;
var FormSection = require('./components/FormSection.jsx').FormSection;
var DragLayer = require('./components/DragLayer.jsx').DragLayer;

var css = require('../../statics/src/styles/main.css');
var css2 = require('../../statics/src/styles/blueprint/print.css');
var css3 = require('../../statics/src/styles/blueprint/screen.css');
var cssCollapse = require('../../statics/src/styles/collapse.css');

// let formDesignerStyles = {

// 	base: {

// 	},

// 	dragHome: {

// 	},

// 	formLayout: {

// 	}
// };	

function getFormDesignerState() {
	return {
		formEntities: FormEntityStore.getFormEntities(),
		appState: AppStateStore.getAppState()
	}
}

var MainApplication =  React.createClass({
	mixins: [PureRenderMixin],

	// !! When serialize, need to set selectedEntityId to 0. !!
	getInitialState: function() {
		return getFormDesignerState();
	},

	componentDidMount: function() {
		FormEntityStore.addChangeListener(this.onChange);
		AppStateStore.addChangeListener(this.onChange);

		window.addEventListener('click', 
			(e) => {
				if (e.target.className !== 'ReactModal__Overlay ReactModal__Overlay--after-open'
					&& e.target.className !== 'ReactModal__Content ReactModal__Content--after-open'
					&& e.target.nodeName !== 'CANVAS'
					&& !this.state.appState.get('addingAssociations')
					&& AppStateStore.getAppState().get('canDrag')) {
					FormDesignerActions.setSelectedEntity(-1);
					FormDesignerActions.setSelectedRegion(-1);
					FormDesignerActions.setWindowFocus(FocusOptions.FORM);
				}
			}
		);

		window.addEventListener('mousedown', 
			(e) => {
				if (e.target.className !== 'ReactModal__Overlay ReactModal__Overlay--after-open'
					&& e.target.className !== 'ReactModal__Content ReactModal__Content--after-open'
					&& e.target.nodeName !== 'CANVAS'
					&& !this.state.appState.get('addingAssociations')) {
					//console.log('mosuedown')
				}
			}
		);

		window.addEventListener('mouseup', 
			(e) => {
				if (e.target.className !== 'ReactModal__Overlay ReactModal__Overlay--after-open'
					&& e.target.className !== 'ReactModal__Content ReactModal__Content--after-open'
					&& e.target.nodeName !== 'CANVAS'
					&& !this.state.appState.get('addingAssociations')) {
					//console.log('mosueup')
				}
			}
		);
	},

	componentWillUnmount: function() {
		FormEntityStore.removeChangeListener(this.onChange);
	},

	onChange: function() {
		this.setState(getFormDesignerState());
	},

	render : function() {
		console.log(this.state.formEntities.toJS());
		console.log(this.state.appState.toJS());
		return (
			<div>
				<StyleRoot>
					<DragHome
						appState={this.state.appState}/>
					<FormLayout
						{...this.state}/>
					<DragLayer/>
				</StyleRoot>
			</div>
		);
	}
});



var DragDropContext = ReactDnD.DragDropContext;
var MainApplicationDnD = DragDropContext(ReactDnDHTMLBackend)(MainApplication);
ReactDOM.render(<MainApplicationDnD/>, document.getElementById('main'));