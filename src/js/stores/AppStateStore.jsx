var AppDispatcher = require('../dispatcher/Dispatcher.js');
var EventEmitter = require('events').EventEmitter;
var Immutable = require('immutable');
var _ = require('underscore');
var HelperFunctions = require('../HelperFunctions.jsx')

var FormDesignerFluxConstants = require('../constants/FormDesignerFluxConstants');
var Constants = require('../constants/Constants.js');
var EntityTypes = Constants.ENTITY_TYPES;
const FocusOptions = Constants.FOCUS_OPTIONS;


let appState = Immutable.fromJS({
	selectedEntityId: -1,
	selectedRegionId: -1,
	canDrag: true,
	addingAssociations: false,
	windowFocus: FocusOptions.FORM,
	// {coords: {top: left:} , dimensions: {width: height:}}
	dependenciesBoxAttributes: {}, 
	validatorsBoxAttributes: {}
});

let setWindowAttributes = function(windowType, property, value) {
	if (windowType == FocusOptions.VALIDATORS) {
		appState = appState.setIn(['validatorsBoxAttributes', property], value);
	} else if (windowType === FocusOptions.DEPENDENCIES) {
		appState = appState.setIn(['dependenciesBoxAttributes', property], value);
	}
}

let setWindowFocus = function(focus) {
	appState = appState.set('windowFocus', focus);
};

let setSelectedEntity = function(entityId) {
	appState = appState.set('selectedEntityId', entityId);
};

let setSelectedRegion = function(regionId) {
	// Entity must already be set, and must be HIB
	appState = appState.set('selectedRegionId', regionId);
}

let enableAddingAssociationsMode = function(entityId) {
	// Entity must already be set, and must be HIB, Region must be already set
	appState = appState.set('addingAssociations', true);
}

let disableAddingAssociationsMode = function() {
	appState = appState.set('addingAssociations', false);
}

let disableDrag = function() {
	appState = appState.set('canDrag', false);
};

let enableDrag = function() {
	appState = appState.set('canDrag', true);
};

let AppStateStore = _.extend({}, EventEmitter.prototype, {
	getAppState: function() {
		return appState;
	},

	emitChange: function() {
		this.emit('change');
	},

	addChangeListener: function(callback) {
		this.on('change', callback);
	},

	removeChangeListener: function(callback) {
		this.removeListener('change', callback);
	}
});

AppDispatcher.register(function(payload){
	var action = payload.action;
	if (payload.source != 'MODIFY_APP_STATE') {return;}

	let emitChange = true;
	switch (action.actionType) {
		case 'ENABLE_ADDING_ASSOCIATIONS_MODE':
			enableAddingAssociationsMode();
			break;
		case 'DISABLE_ADDING_ASSOCIATIONS_MODE':
			disableAddingAssociationsMode();
			break;
		case 'SET_SELECTED_ENTITY':
			setSelectedEntity(action.entityId);
			break;
		case 'SET_SELECTED_REGION':
			setSelectedRegion(action.regionId);
			break;
		case 'DISABLE_DRAG':
			disableDrag();
			emitChange = false;
			break;
		case 'ENABLE_DRAG':
			enableDrag();
			emitChange = false;
			break;
		case 'SET_WINDOW_FOCUS':
			setWindowFocus(action.focus);
			break;
		case 'SET_WINDOW_COORDINATES':
			setWindowAttributes(action.windowType, action.property, action.value);
			break;
		default:
			return true;
	}
	if (emitChange) {
		AppStateStore.emitChange();
	}
});

module.exports = AppStateStore;
