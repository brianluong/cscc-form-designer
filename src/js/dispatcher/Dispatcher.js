var Dispatcher = require('flux').Dispatcher;
var AppDispatcher = new Dispatcher();
/*
	So it seems that side by side dispatcher events are executed in order, synchronously,
	but doesn't rerender. Rerendering is only done when all the dispatcher events are done
*/

AppDispatcher.changeAppState = function(action) {
	this.dispatch({
		source: 'MODIFY_APP_STATE',
		action: action
	})
};

AppDispatcher.handleModifyLayout = function(action) {
	//console.log('dispatching ... ' + action.actionType)
	this.dispatch({
		source: 'MODIFY_LAYOUT',
		action: action
	});
}

// DROP EVENT VALIDATED BY FORMSECTION
// IF VALID, THEN ACTION PASSED TO AppDispatcher.handleModifyLayout
AppDispatcher.validateDropFormEntity = function(action) {
	this.dispatch({
		source: 'FORM_ENTITY_WRAPPER',
		action: action
	})
}

AppDispatcher.validateResizeFormEntity = function(action) {
	this.dispatch({
		source: 'FORM_ENTITY_RESIZE',
		action: action
	});
}

AppDispatcher.serialization = function(action) {
	this.dispatch({
		source: 'SERIALIZATION',
		action: action
	})
}

AppDispatcher.setSelectedEntity = function(action) {
	this.dispatch({
		source: 'MODIFY_APP_STATE',
		action: action
	})
}

AppDispatcher.handleHighlightRegion = function(action) {
	this.dispatch({
		source: 'MODIFY_LAYOUT',
		action: action
	})
}

module.exports = AppDispatcher;