var AppDispatcher = require('../dispatcher/Dispatcher.js');
var FormDesignerFluxConstants = require('../constants/FormDesignerFluxConstants.js');

module.exports = {
	setWindowAttributes: function(windowType, property, value) {
		AppDispatcher.changeAppState({
			windowType: windowType,
			property: property,
			value: value,
			actionType: 'SET_WINDOW_COORDINATES'
		})
	},

	setWindowFocus: function(focus) {
		AppDispatcher.changeAppState({
			focus: focus,
			actionType: 'SET_WINDOW_FOCUS'
		});
	},

	removeDependenciesWithId: function(entityId) {
		AppDispatcher.handleModifyLayout({
			entityId: entityId,
			actionType: 'REMOVE_DEPENDENCIES'
		});
	},

	updateFormEntityProperty: function(entityId, keyPath, value) {
		AppDispatcher.handleModifyLayout({
			entityId: entityId,
			keyPath: keyPath,
			value: value,
			actionType: 'UPDATE_FORM_ENTITY_PROPERTY'
		})
	},

	enableAddingAssociationsMode: function(entityId) {
		AppDispatcher.changeAppState({
			actionType: 'ENABLE_ADDING_ASSOCIATIONS_MODE'
		});
	},

	disableAddingAssociationsMode: function(entityId) {
		AppDispatcher.changeAppState({
			actionType: 'DISABLE_ADDING_ASSOCIATIONS_MODE'
		});
	},

	setSelectedEntity: function(entityId) {
		AppDispatcher.changeAppState({
			entityId: entityId,
			actionType: 'SET_SELECTED_ENTITY'
		});
	},

	setSelectedRegion: function(regionId) {
		AppDispatcher.changeAppState({
			regionId: regionId,
			actionType: 'SET_SELECTED_REGION'
		})
	},

	enableDrag: function() {
		AppDispatcher.changeAppState({
			actionType: 'ENABLE_DRAG'
		});
	},	

	disableDrag: function() {
		AppDispatcher.changeAppState({
			actionType: 'DISABLE_DRAG'
		});
	},










	toggleAssociation: function(entityId) {
		AppDispatcher.handleModifyLayout({
			id: entityId,
			actionType: 'TOGGLE_ASSOCIATION'
		})
	},

	toggleRegionVisibility: function(id, data) {
		AppDispatcher.handleHighlightRegion({
			id: id,
			regionId: regionId,
			actionType: FormDesignerFluxConstants.FORM_HIGHLIGHT_REGION_TOGGLE_VISIBILITY
		})
	},

	updateFormSection: function(id, childEntities) {
		AppDispatcher.handleModifyLayout({
			id: id,
			childEntities: childEntities,
			actionType: FormDesignerFluxConstants.FORM_UPDATE_FORM_SECTION
		});
	},

	updateFormDesigner: function(data) {
		AppDispatcher.handleModifyLayout({
			data: data,
			actionType: FormDesignerFluxConstants.FORM_DESIGNER_UPDATE
		});
	},

	addTab: function() {
		AppDispatcher.handleModifyLayout({
			actionType: FormDesignerFluxConstants.FORM_ADD_TAB
		});
	},

	reorderTabs: function(dragIndex, hoverIndex) {
		AppDispatcher.handleModifyLayout({
			dragIndex: dragIndex,
			hoverIndex: hoverIndex,
			actionType: FormDesignerFluxConstants.FORM_REORDER_TABS,
		});
	},

	updateFormEntity: function(id, data) {
		AppDispatcher.handleModifyLayout({
			id: id,
			data: data,
			actionType: FormDesignerFluxConstants.FORM_ENTITY_UPDATE
		});
	},

	updateFormRow: function(row) {
		AppDispatcher.handleModifyLayout({
			row: row,
			actionType: FormDesignerFluxConstants.FORM_UPDATE_ROW
		});
	},

	removeFormEntity: function(id) {
		AppDispatcher.handleModifyLayout({
			id: id,
			actionType: FormDesignerFluxConstants.FORM_ENTITY_REMOVE
		})
	},
	
	serialize: function() {
		AppDispatcher.serialization({
			actionType: 'SERIALIZE'
		})
	},

	deserialize: function(json) {
		AppDispatcher.serialization({
			actionType: 'DESERIALIZE',
			json: json
		})
	}
}