var Immutable = require('immutable');

/*
	To find the path to a form entity inside a nested ImmutableJS structure
	@param {id} the ID of the form entity searching
	@param {formEntities} an ImmutableJS List of form entities
	@param {result} an object {found: false, path: []}
*/

const getFormEntityPathToId = function(id, formEntities) {
	return Immutable.fromJS(getFormEntityPathToIdHelper(id, formEntities, {found: false, path: []}).path);
}

const getFormEntityPathToIdHelper = function(id, formEntities, result) {
// BFS search to get path to FormEntity
	var nextLevel = [];
	for (var i = 0; i < formEntities.size; i++) {
		var formEntity = formEntities.get(i);
		if (id === formEntity.get('id')) {
			result.path.unshift(i);
			result.found = true;
			return result;
		}
		if (formEntity.get('entityType') === 'FormSection') {
			nextLevel.push({formEntity: formEntity, position: i});
		}
	}

	for (var i = 0; i < nextLevel.length; i++) {
		var result = getFormEntityPathToIdHelper(id, nextLevel[i].formEntity.get('childEntities'), result);
		if (result.found) {
			result.path.unshift('childEntities');
			result.path.unshift(nextLevel[i].position);
			return result;
		}
	}
	return result;
}

module.exports = {
	getFormEntityPathToId: getFormEntityPathToId
}