var React = require('react');
var ReactDOM = require('react-dom');
var ReactDnD = require('react-dnd');
var $ = require('jquery');
var autosize = require('autosize');

var FormDesignerActions = require('../actions/FormDesignerActions.js');
var AppStateStore = require('../stores/AppStateStore.jsx');

var TextAreaAutoResize = React.createClass({
	/*
		Seems like onChange and componentDidUpdate are bothed fired when textArea changes
	*/
	onChange: function(e) {
		var newText = e.target.value;
		if (this.props.type === 'promptPre' || this.props.type === 'promptPost') {
			FormDesignerActions.updateFormEntity(this.props.id, {[this.props.type + 'Text']: newText});
		} else if (this.props.type === 'label' || this.props.type === 'value') {
			this.props.onChange(this.props.type, newText);
		} else if (this.props.type === 'textarea') {
			FormDesignerActions.updateFormEntity(this.props.id, {text: newText});
		} else if (this.props.type === 'textBlock') {
			FormDesignerActions.updateFormEntity(this.props.id, {content: newText});
		}
		this.updateTextArea(this.props.text);
		this.forceUpdate();
	},

	updateTextArea: function(text) { 
		let ta = ReactDOM.findDOMNode(this.refs.textarea);
		autosize(ta);
		var evt = document.createEvent('Event');
		evt.initEvent('autosize:update', true, false);
		ta.dispatchEvent(evt);
	},

	componentDidMount: function() {
		this.updateTextArea();
	},

	componentDidUpdate: function() {
		this.updateTextArea();
	},

	onMouseEnter: function() { 
		FormDesignerActions.disableDrag();
	},

	onMouseLeave: function() {
		FormDesignerActions.enableDrag();
	},

	render: function() {
		let disabled = false;
		let background;
		if (this.props.type === 'body') {
			disabled = true;
		}

		let textAreaStyle = {
			width:'100%',
			boxSizing: 'border-box',
			border: '1px gray solid',
			background: 'rgb(255,255,255)'
		};

		if (this.props.backgroundColor) {
			textAreaStyle.backgroundColor = this.props.backgroundColor;
		}

		if (disabled) {
			textAreaStyle.backgroundColor = 'rgb(128,128,128)';
		}

		return (
			<textarea
				rows='1'
				ref='textarea'
				disabled={disabled}
				style={textAreaStyle}
				value={this.props.text ? this.props.text : ''}
				onChange={this.onChange}
				onMouseDown={this.onMouseDown}
				onMouseUp={this.onMouseUp}
				onMouseEnter={this.onMouseEnter}
				onMouseLeave={this.onMouseLeave}
				/>
		);
	}
});

module.exports = {
	TextAreaAutoResize: TextAreaAutoResize
}