var React = require('react');
var _ = require('underscore');

var FormDesignerActions = require('../actions/FormDesignerActions.js');
let MultiSelectList = require('./MultiSelectList.jsx').MultiSelectList;

let Select = require('react-select');
let requireStyles = require('react-select/dist/react-select.css');

let Constants = require('../constants/Constants.js');

const timeZoneFormattedForSelect = _.reduce(Constants.TIME_ZONES, (memo, value, key) =>{ memo.push({label: key, value: key}); return memo}, []);

const DateTimeAttributes = React.createClass({

	render: function() {
		return (
			<div>
				<div
					style={{position: 'relative'}}>
					<div
						style={{float: 'left'}}>
						Precision Options
						<MultiSelectList
							id={this.props.entity.get('id')}
							optionsList={this.props.entity.get('precisionOptions')}/>
					</div>

					<div
						style={{float: 'left'}}>
						<div
							style={{position: 'absolute', top: '50%'}}>
							<input
								type='checkbox'
								checked={this.props.entity.get('allDateComponentsRequired')}
								onChange={() => 
									FormDesignerActions.updateFormEntity(this.props.entity.get('id'), 
										{
											allDateComponentsRequired: !this.props.entity.get('allDateComponentsRequired')
										})}/>
							All date components required
						</div>
					</div>

					<div style={{clear: 'both'}}/>
				</div>


				

				<div>
					Time Zone
					<Select
						options={timeZoneFormattedForSelect}
						value={this.props.entity.get('timeZone')}
						onChange={(value) =>
							FormDesignerActions.updateFormEntity(this.props.entity.get('id'), 
								{
									timeZone: value.value
								})
						}
						searchable
						resetValue={{value: "Unknown/Unknown"}}/>
				</div>



				<div>
					<input
						type='checkbox'
						checked={this.props.entity.get('displayTimeZoneOptions')}
						onChange={() => 
							FormDesignerActions.updateFormEntity(this.props.entity.get('id'), 
								{
									displayTimeZoneOptions: !this.props.entity.get('displayTimeZoneOptions')
								})}/>
					Display Time Zone options
				</div>
			</div>
		);
	}
});


module.exports = {
	DateTimeAttributes: DateTimeAttributes
}