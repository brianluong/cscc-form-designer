var React = require('react');
var ReactDnD = require('react-dnd');
var BlueprintWidths = require('../constants/blueprint-widths.js');
var getEmptyImage = require('react-dnd-html5-backend').getEmptyImage;
var Resizable = require('react-resizable-box').default;
var ResizableBox = require('react-resizable').ResizableBox;
var TextareaAutosize = require('react-autosize-textarea');
var ReactDOM = require('react-dom');
var Immutable = require('immutable');
var Radium = require('radium');	

var FormDesignerActions = require('../actions/FormDesignerActions.js');
var SelectPortal = require('./SelectDropdown.jsx').SelectPortal;

var Constants = require('../constants/Constants.js');
var EntityTypes = Constants.ENTITY_TYPES;
var DnDTypes = Constants.DND_TYPES;


var FormSectionIcon = Radium(React.createClass({
	getDefaultProps: function() {
		return {
			width: 6,
			canDrop: true
		}
	},

	componentDidMount: function() {
    	this.props.connectDragPreview(getEmptyImage());
	},

	render: function() {
		var connectDragSource = this.props.connectDragSource;
		var connectDragPreview = this.props.connectDragPreview;
		return (
			<div
				style={[dragHomeStyle.container]}>
				Form Section
				{connectDragSource(
				<div
					style={[dragHomeStyle.baseFormEntity, dragHomeStyle.formSection, {width: BlueprintWidths[this.props.width]}]}/>
				)}
			</div>
		);
	}
}));

var formSectionIconSpec = {
	beginDrag: function(props, monitor, component) {
		return {
			id: -1,
			width: props.width,
			prepend: 0,
			append: 0,
			entityType: EntityTypes.FORM_SECTION,
			childEntities: Immutable.fromJS([{width: props.width, entityType: EntityTypes.FILLER, 'last': true}])
		};
	},

	canDrag: function(props, monitor) {
		if (props.appState.get('addingAssociations')) {
			return false;
		}
		return true;
	}
};

var TextInputIcon = Radium(React.createClass({
	componentDidMount: function() {
    	this.props.connectDragPreview(getEmptyImage());
	},

	render: function() {
		var connectDragSource = this.props.connectDragSource;
		return (
			<div
				style={[dragHomeStyle.container]}>
				Text Input
				{connectDragSource(
				<div
					style={[dragHomeStyle.baseFormEntity, dragHomeStyle.textInput, {width: BlueprintWidths[this.props.width]}]}/>
				)}
			</div>
		);
	}
}));

var textInputIconSpec = {
	beginDrag: function(props, monitor, component) {
		FormDesignerActions.setSelectedEntity(-1);
		FormDesignerActions.setSelectedRegion(-1);
		return {
			id: -1,
			width: props.width,
			promptPreText: '',
			promptPreWidth: 1,
			promptPostText: '',
			promptPostWidth: 1,
			prepend: 0,
			append: 0,
			entityType: EntityTypes.TEXT_INPUT
		};
	},

	canDrag: function(props, monitor) {
		if (props.appState.get('addingAssociations')) {
			return false;
		}
		return true;
	}
};

var TextAreaIcon = Radium(React.createClass({
	componentDidMount: function() {
    	this.props.connectDragPreview(getEmptyImage());
	},

	render: function() {
		var connectDragSource = this.props.connectDragSource;
		return (
			<div
				style={[dragHomeStyle.container]}>
				Text Area Input
				{connectDragSource(
					<div
						style={[dragHomeStyle.baseFormEntity, dragHomeStyle.textAreaInput, {width: BlueprintWidths[this.props.width]}]}/>
				)}
			</div>
		);
	}
}));

var textareaIconSpec = {
	beginDrag: function(props, monitor, component) {
		FormDesignerActions.setSelectedEntity(-1);
		FormDesignerActions.setSelectedRegion(-1);
		return {
			id: -1,
			width: props.width,
			promptPreText: '',
			promptPreWidth: 1,
			promptPostText: '',
			promptPostWidth: 1,
			prepend: 0,
			append: 0,
			rows: 1,
			entityType: EntityTypes.TEXTAREA_INPUT
		};
	},

	canDrag: function(props, monitor) {
		if (props.appState.get('addingAssociations')) {
			return false;
		}
		return true;
	}
};

var SelectInputIcon = Radium(React.createClass({
	componentDidMount: function() {
    	this.props.connectDragPreview(getEmptyImage());
	},

	render: function() {
		var connectDragSource = this.props.connectDragSource;
		return (
			<div
				style={[dragHomeStyle.container]}>
				Select Input
				{connectDragSource(
					<div
						style={[dragHomeStyle.baseFormEntity, dragHomeStyle.selectInput, {width: BlueprintWidths[this.props.width]}]}/>
				)}
			</div>
		);
	}
}));

var selectInputIconSpec = {
	beginDrag: function(props, monitor, component) {
		FormDesignerActions.setSelectedEntity(-1);
		FormDesignerActions.setSelectedRegion(-1);
		return {
			id: -1,
			width: props.width,
			promptPreText: '',
			promptPreWidth: 1,
			promptPostText: '',
			promptPostWidth: 1,
			prepend: 0,
			append: 0,
			selectionOptions: Immutable.fromJS([{label: '', value: ''}]),
			entityType: EntityTypes.SELECT_INPUT
		};
	},

	canDrag: function(props, monitor) {
		if (props.appState.get('addingAssociations')) {
			return false;
		}
		return true;
	}
};

var CheckboxIcon = Radium(React.createClass({
	componentDidMount: function() {
    	this.props.connectDragPreview(getEmptyImage());
	},

	render: function() {
		var connectDragSource = this.props.connectDragSource;
		return (
			<div
				style={[dragHomeStyle.container]}>
				Checkbox Input
				{connectDragSource(
					<div
						style={[dragHomeStyle.baseFormEntity, dragHomeStyle.checkboxInput, {width: BlueprintWidths[this.props.width]}]}/>
				)}
			</div>
		);
	}
}));

var checkboxIconSpec = {
	beginDrag: function(props, monitor, component) {
		FormDesignerActions.setSelectedEntity(-1);
		FormDesignerActions.setSelectedRegion(-1);
		return {
			id: -1,
			width: props.width,
			promptPreText: '',
			promptPreWidth: 1,
			promptPostText: '',
			promptPostWidth: 1,
			prepend: 0,
			append: 0,
			defaultChecked: false,
			entityType: EntityTypes.CHECKBOX_INPUT
		};
	},

	canDrag: function(props, monitor) {
		if (props.appState.get('addingAssociations')) {
			return false;
		}
		return true;
	}
};

let DateTimeIcon = Radium(React.createClass({
	componentDidMount: function() {
		this.props.connectDragPreview(getEmptyImage());
	},

	render: function() {
		var connectDragSource = this.props.connectDragSource;
		return (
			<div
				style={[dragHomeStyle.container]}>
				Date Time Input
				{connectDragSource(
					<div
						style={[dragHomeStyle.baseFormEntity, dragHomeStyle.dateTimeInput, {width: BlueprintWidths[this.props.width]}]}/>
				)}
			</div>
		);
	}
}));

let dateTimeIconSpec = {
	beginDrag: function(props, monitor, component) {
		FormDesignerActions.setSelectedEntity(-1);
		FormDesignerActions.setSelectedRegion(-1);
		return {
			id: -1,
			width: props.width,
			promptPreText: '',
			promptPreWidth: 1,
			promptPostText: '',
			promptPostWidth: 1,
			prepend: 0,
			append: 0,
			entityType: EntityTypes.DATETIME_INPUT
		};
	},

	canDrag: function(props, monitor) {
		if (props.appState.get('addingAssociations')) {
			return false;
		}
		return true;
	}
};

var TextBlockIcon = Radium(React.createClass({
	componentDidMount: function() {
    	this.props.connectDragPreview(getEmptyImage());
	},

	render: function() {
		var connectDragSource = this.props.connectDragSource;
		return (
			<div
				style={[dragHomeStyle.container]}>
				Text Block
				{connectDragSource(
					<div
						style={[dragHomeStyle.baseFormEntity, dragHomeStyle.textBlock, {width: BlueprintWidths[this.props.width]}]}/>
				)}
			</div>
		);
	}
}));

var textBlockIconSpec = {
	beginDrag: function(props, monitor, component) {
		FormDesignerActions.setSelectedEntity(-1);
		FormDesignerActions.setSelectedRegion(-1);
		return {
			id: -1,
			width: props.width,
			prepend: 0,
			append: 0,
			entityType: EntityTypes.TEXTBLOCK
		}
	},

	canDrag: function(props, monitor) {
		if (props.appState.get('addingAssociations')) {
			return false;
		}
		return true;
	}
};

var ImageBlockIcon = Radium(React.createClass({
	componentDidMount: function() {
    	this.props.connectDragPreview(getEmptyImage());
	},

	render: function() {
		var connectDragSource = this.props.connectDragSource;
		return (
			<div
				style={[dragHomeStyle.container]}>
				Image Block
				{connectDragSource(
					<div
						style={[dragHomeStyle.baseFormEntity, dragHomeStyle.imageBlock, {width: BlueprintWidths[this.props.width]}]}/>
				)}
			</div>
		);
	}
}));

var imageBlockIconSpec = {
	beginDrag: function(props, monitor, component) {
		FormDesignerActions.setSelectedEntity(-1);
		FormDesignerActions.setSelectedRegion(-1);
		return {
			id: -1,
			width: props.width,
			prepend: 0,
			append: 0,
			// imgUrl: 'statics/dog.jpg',
			// aspectRatio: 1.93,
			entityType: EntityTypes.IMAGEBLOCK
		}
	},

	canDrag: function(props, monitor) {
		if (props.appState.get('addingAssociations')) {
			return false;
		}
		return true;
	}
};

var PromptIcon = Radium(React.createClass({
	componentDidMount: function() {
    	this.props.connectDragPreview(getEmptyImage());
	},

	render: function() {
		var connectDragSource = this.props.connectDragSource;
		return (
			<div
				style={[dragHomeStyle.container]}>
				Post / Pre Prompt
				{connectDragSource(
					<div
						style={[dragHomeStyle.baseFormEntity, dragHomeStyle.prePostPrompt, {width: BlueprintWidths[this.props.width]}]}/>
				)}
			</div>
		);
	}
}));

var promptIconSpec = {
	beginDrag: function(props, monitor, component) {
		FormDesignerActions.setSelectedEntity(-1);
		FormDesignerActions.setSelectedRegion(-1);
		return {
			id: -1,
			width: props.width,
			text: '',
			entityType: EntityTypes.PROMPT
		};
	},

	canDrag: function(props, monitor) {
		if (props.appState.get('addingAssociations')) {
			return false;
		}
		return true;
	}
};

var formEntityIconCollect = function(connect, monitor) {
	return {
		connectDragSource: connect.dragSource(),
		connectDragPreview: connect.dragPreview()
	}
};

var DragSource= ReactDnD.DragSource;
var FormSectionIconDnD = DragSource(DnDTypes.TEXT_BOX, formSectionIconSpec, formEntityIconCollect)(FormSectionIcon);
var TextInputIconDnD = DragSource(DnDTypes.TEXT_BOX, textInputIconSpec, formEntityIconCollect)(TextInputIcon);
var TextareaIconDnD = DragSource(DnDTypes.TEXT_BOX, textareaIconSpec, formEntityIconCollect)(TextAreaIcon);
var SelectInputIconDnD = DragSource(DnDTypes.TEXT_BOX, selectInputIconSpec, formEntityIconCollect)(SelectInputIcon);
var CheckboxIconDnD = DragSource(DnDTypes.TEXT_BOX, checkboxIconSpec, formEntityIconCollect)(CheckboxIcon);
var DateTimeIconDnD = DragSource(DnDTypes.TEXT_BOX, dateTimeIconSpec, formEntityIconCollect)(DateTimeIcon);
var ImageBlockDnD = DragSource(DnDTypes.TEXT_BOX, imageBlockIconSpec, formEntityIconCollect)(ImageBlockIcon);
var TextBlockDnD = DragSource(DnDTypes.TEXT_BOX, textBlockIconSpec, formEntityIconCollect)(TextBlockIcon);
var PromptIconDnD = DragSource(DnDTypes.PROMPT, promptIconSpec, formEntityIconCollect)(PromptIcon);

let dragHomeStyle = {

	dragHome: {
		border: '1px solid black',
		overflow: 'auto',

		'@media (max-width: 1208px)': {
			float: 'top',
			height: '65px',
			minWidth: '950px',
			width: '100%'
		},

		'@media (min-width: 1209px)': {
			float: 'left',
			height: '99vh',
			minHeight: '500px',
			width: '230px'
		},
	},

	container: {
		'@media (max-width: 1208px)': {
			float: 'left',
			paddingRight: '4px'
		},

		'@media (min-width: 1209px)': {
			float: 'none'
		},
	},

	baseFormEntity: {
		height: '39px',
		cursor: 'move',
	},

	formSection: {
		background: 'rgba(132, 112, 225, .5)'
	},

	textInput: {
		background: 'rgba(145, 12, 12, .5)'
	}, 

	textAreaInput: {
		background: 'rgba(86, 24, 171, .5)'
	},

	selectInput: {
		background: 'rgba(5, 96, 208, .5)'
	},

	checkboxInput: {
		background: 'rgba(7, 206, 77, .5)'
	},

	dateTimeInput: {
		background: 'rgba(139, 69, 19, .5)'
	},

	imageBlock: {
		background: 'rgba(34,36,123, .5)'
	},

	textBlock: {
		background: 'rgba(69, 9, 37, .5)'
	},

	prePostPrompt: {
		background: 'rgba(249, 244, 98, .5)'
	}
};


let DraggableElementsContainer = React.createClass({
		render: function() {

			return (
				<div
					style={dragHomeStyle.dragHome}>
					<FormSectionIconDnD
						width={6}
						appState={this.props.appState}/>
					
					<TextInputIconDnD
						width={4}
						appState={this.props.appState}/>
					
					<TextareaIconDnD
						width={4}
						appState={this.props.appState}/>
					
					<SelectInputIconDnD
						width={4}
						appState={this.props.appState}/>
				
					<CheckboxIconDnD
						width={2}
						appState={this.props.appState}/>

					<DateTimeIconDnD
						width={2}
						appState={this.props.appState}/>

					<ImageBlockDnD
						width={2}
						appState={this.props.appState}/>

					<TextBlockDnD
						width={2}
						appState={this.props.appState}/>

					<PromptIconDnD
						width={1}
						appState={this.props.appState}/>
				</div>
			);
		}
})
module.exports = {
	DraggableElementsContainer:  Radium(DraggableElementsContainer)
};