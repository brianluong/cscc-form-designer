var React = require('react');
var ReactDnD = require('react-dnd');
var ReactDOM = require('react-dom');
var PureRenderMixin = require('react-addons-pure-render-mixin');
var Immutable = require('immutable');
var NativeTypes = require('react-dnd-html5-backend').NativeTypes;
var $ = require('jquery');
var _ = require('underscore');

var FormDesignerActions = require('../actions/FormDesignerActions.js');
var FormEntityStore = require('../stores/FormEntityStore.js');
var FormEntity = require('./FormEntity.jsx').FormEntity;
var FormSection = require('./FormSection.jsx').FormSection;
var TextInput = require('./TextInput.jsx').TextInput;
var TextareaInput = require('./TextAreaInput.jsx').TextareaInput;
var SelectInput = require('./SelectInput.jsx').SelectInput;
var CheckboxInput = require('./CheckboxInput.jsx').CheckboxInput;
let DateTimeInput = require('./DateTimeInput.jsx').DateTimeInput;
let TextBlock = require('./TextBlock.jsx').TextBlock;
var ImageBlock = require('./ImageBlock.jsx').ImageBlock;
var HighlightImageBlock = require('./HighlightImageBlock.jsx').HighlightImageBlock;

var BlueprintWidths = require('../constants/blueprint-widths.js');

var Constants = require('../constants/Constants.js');
var EntityTypes = Constants.ENTITY_TYPES;
var DnDTypes = Constants.DND_TYPES;

var FormInputFactory = require('./FormInputFactory.jsx').FormInputFactory;

var TextAreaAutoResize = require('./TextAreaAutoResize.jsx').TextAreaAutoResize;
var SelectPortal = require('./SelectDropdown.jsx').SelectPortal;
var Checkbox = require('./Checkbox.jsx').Checkbox;
var TextAreaResizable = require('./TextAreaResizable.jsx').TextAreaResizable;
var DateTime = require('./DateTime.jsx').DateTime;

var spec = {
	drop: function(props, monitor, component) {
		if (monitor.didDrop()) {
			return;
		}

		// Mouse Cursor dropped over an Entity
		var node = ReactDOM.findDOMNode(component).getBoundingClientRect();
		var cursorX = monitor.getClientOffset();
		// if (cursorX > node.left + (props.prepend * Constants.PREPEND_WIDTH)
		// 	&& cursorX < node.right - props.append * Constants.APPEND_WIDTH) {
		// 	// dropping inside width of entity
		// 	return;
		// }
		var item = monitor.getItem();
		var X =  monitor.getClientOffset().x;	

		if (item.hasOwnProperty('files')) {
			item.entityType = EntityTypes.IMAGEBLOCK;
			item.width = 5;
			item.id = -1;
			item.altText = '';
			item.title = '';
			item.file = item.files[0];
			item.fromLocal = true;
		}
		props.dropFormEntity(item, props.entity.toJS(), X, node);
	},

	hover: function(props, monitor, component) {
		let entity = props.entity;
		if (monitor.isOver({shallow: true}) || monitor.getItem().id === entity.get('id')) {
			var node = ReactDOM.findDOMNode(component).getBoundingClientRect();
			var cursorX = monitor.getClientOffset();
			if (cursorX > node.left + (entity.get('prepend') * Constants.PREPEND_WIDTH)
				&& cursorX < node.right - entity.get('append') * Constants.APPEND_WIDTH) {
				return;
			}

			var item = monitor.getItem();
			var X =  monitor.getClientOffset().x;
			props.isValidDrop(item, props.entity.toJS(), X, node);
		}
	},

	canDrop: function(props, monitor) {
		// Ensure that a parent Form Section won't be dropped into a child wrapper
		if (monitor.getItem().entityType === EntityTypes.FORM_SECTION) {
			var childEntities = monitor.getItem().childEntities;
			if (_.find(childEntities.toJS(), function(entity){return entity.id === props.entity.get('id')}) != undefined) {
				return false;
			}
		}
		return true;
	}
};

var collect = function(connect, monitor) {
	return {
		connectDropTarget: connect.dropTarget(),
		canDrop: monitor.canDrop()
	}
};

var FormEntityWrapper = React.createClass({
	mixins: [PureRenderMixin],

	componentDidMount: function() {
		FormEntityStore.addChangeListener(this.onChange);
	},

	getInitialState: function() {
		return {addingAssociationsSelected: false};
	},

	// Listen to change in FormEntityStore
	onChange: function() {
		if (this.props.appState.get('addingAssociations')) {
			let selectedEntityId = this.props.appState.get('selectedEntityId');
			let selectedRegionId = this.props.appState.get('selectedRegionId');
			let associatedFormEntities = FormEntityStore.getFormEntity(selectedEntityId)
				.getIn(['regions', selectedRegionId, 'associations']);
			console.log(associatedFormEntities.toJS())
			if (associatedFormEntities.includes(this.props.entity.get('id'))) {
				this.setState({addingAssociationsSelected: true});
			} else {
				this.setState({addingAssociationsSelected: false});
			}
		}
	},

	generateFormInputPreview: function(formEntity) {
		let className;
		let bodyPreview;
		if (formEntity.entityType === EntityTypes.TEXT_INPUT) {
			className = 'textInput';
			bodyPreview = <TextAreaAutoResize/>
		} else if (formEntity.entityType === EntityTypes.TEXTAREA_INPUT) {
			className = 'textareaInput';
			bodyPreview = <TextAreaResizable
							text={formEntity.text}
							width={formEntity.width}
							rows={formEntity.rows}
							rowHeight={13}/>
		} else if (formEntity.entityType === EntityTypes.SELECT_INPUT) {
			className = 'selectInput';
			bodyPreview = <SelectPortal
							selectionOptions={formEntity.selectionOptions}/>
		} else if (formEntity.entityType === EntityTypes.CHECKBOX_INPUT) {
			className = 'checkboxInput';
			bodyPreview = <Checkbox 
							defaultChecked={formEntity.defaultChecked}
							readOnly/>;
		}  else if (formEntity.entityType === EntityTypes.DATETIME_INPUT) {
			className = 'dateTimeInput';
			bodyPreview = <DateTime/>
		}

		return (
			 <div 
				className={'span-' + (formEntity.width + formEntity.promptPreWidth + formEntity.promptPostWidth)}
				style={{background: 'rgba(204,158,228,.5)', opacity: 0}}>
				<div
					className={formEntity.promptPreWidth > 0 ? ('prompt ' + 'span-' + formEntity.promptPreWidth) : ''}
					style={{float: 'left'}}>
					<TextAreaAutoResize text={formEntity.promptPreText}/>
				</div>
				<div
					className={className + ' span-' + formEntity.width}>
					{bodyPreview}
				</div>
				<div
					className={'prompt ' + 'span-' + formEntity.promptPostWidth}
					style={{marginRight: 0}}>
					<TextAreaAutoResize 
						text={formEntity.promptPostText}/>
				</div>
			</div>
		);
	},

	height: 0,

	// componentDidMount: function() {
	// //	this.height = ReactDOM.findDOMNode(this.entityRef).getBoundingClientRect().height;
	// },

	componentDidUpdate: function(prevProps, prevState) {
		// if (this.entityRef) {
		// 	this.height = ReactDOM.findDOMNode(this.entityRef).getBoundingClientRect().height;
		// }
	},

	componentWillReceiveProps: function(nextProps) {
		if (nextProps.hoveredWrapper 
			&& this.props.hoveredWrapper
			&& nextProps.hoveredWrapper.hoveringEntity === this.props.hoveredWrapper.hoveringEntity) {
			// Do nothing
		} else if (nextProps.hoveredWrapper 
					&& !this.props.hoveredWrapper) {
			// set state
			this.setState({formEntityPreview : this.generateFormInputPreview(nextProps.hoveredWrapper.hoveringEntity)});
		} else if (!nextProps.hoveredWrapper
					&& this.props.hoveredWrapper) {
			// reset state
			this.setState({formEntityPreview : null});
		}
	},

	render: function() {
		var connectDropTarget = this.props.connectDropTarget;
		var highlightedBlock;
		var hoveredWrapperBefore;
		let hoveredWrapperBefore2;
		let hoveredWrapperBefore3;
		var hoveredWrapperAfter;
		var hoveredWrapperBottom;

		const entity = this.props.entity;

		let width = entity.get('width');
		let prepend = entity.get('prepend');
		let append = entity.get('append');
		let entityType = entity.get('entityType');
		let last = entity.get('last');

		if (entityType === EntityTypes.TEXT_INPUT
			|| entityType === EntityTypes.TEXTAREA_INPUT
			|| entityType === EntityTypes.SELECT_INPUT
			|| entityType === EntityTypes.CHECKBOX_INPUT
			|| entityType === EntityTypes.DATETIME_INPUT) {
			width += entity.get('promptPreWidth') + entity.get('promptPostWidth');
		}

		let fillerStyle;
		var ghostDiv; // used to 'pad' because wrappers are absolutely positioned, so row will collapse
		let hoveredWrapper = this.props.hoveredWrapper;
		if (hoveredWrapper) {
		//	console.log(hoveredWrapper.location)
				//console.log(this.props)
			//console.log(hoveredWrapper)
			if (hoveredWrapper.location === 'prepend') {
				if (!hoveredWrapper.collapse) { // collapse when its second hoverwrapper when its (hover over itself)
					//console.log('not collapsing')
					//console.log('the prepend ' + this.props.prepend)
					hoveredWrapperBefore = <div style={{width: BlueprintWidths[prepend],
														float: 'left',
														marginRight: 10,
														position: 'absolute',
														//overflow: 'hidden',
														top: 0,
														bottom:0 ,
														pointerEvents: 'none'}}
												className={hoveredWrapper.validDrop ? 'valid' : 'invalid'}>
												
											</div>;
					// if (hoveredWrapper.validDrop) {
					// 	hoveredWrapperBefore2 = <div
					// 							style={{width: BlueprintWidths[prepend],
					// 									float: 'left',
					// 									marginRight: 10,
					// 									pointerEvents: 'none'}}>
					// 							<div style={{opacity: 0}}>
					// 								{this.generateFormInputPreview(hoveredWrapper.hoveringEntity)}
					// 							</div>
					// 						</div>
					// } else {
						ghostDiv = <div style={{width: BlueprintWidths[prepend],
													float: 'left',
													marginRight: 10,
													height: '1px',
													opacity: 0}}>
								</div>;
				//	}
					width += prepend;
				} else {
					prepend = 0;
				}
			} else if (hoveredWrapper.location === 'append') {
				hoveredWrapperAfter = <div style={{marginLeft: BlueprintWidths[width] + 10,
													width: BlueprintWidths[append], 
													height: '100%',
													position: 'absolute',
													top: 0,
													bottom: 0,
													pointerEvents: 'none'}}
											className={hoveredWrapper.validDrop ? 'valid' : 'invalid'}>
										</div>;

				// if (hoveredWrapper.validDrop) {
				// 		hoveredWrapperBefore3 = <div
				// 									style={{width: BlueprintWidths[append],
				// 											float: 'left',
				// 											marginRight: 10,
				// 											pointerEvents: 'none'}}>
				// 									<div style={{opacity: 0}}>
				// 										{this.generateFormInputPreview(hoveredWrapper.hoveringEntity)}
				// 								</div>
				// 							</div>
				// }
				width += append;
			} else if (hoveredWrapper.location === 'self') {
				width += hoveredWrapper.additionalWidth;
				// hoveredWrapperBefore3 = <div
				// 							style={{
				// 									float: 'left',
				// 									marginRight: 10,
				// 									pointerEvents: 'none'}}>
				// 							<div style={{opacity: 0}}>
				// 								{this.generateFormInputPreview(hoveredWrapper.hoveringEntity)}
				// 							</div>
				// 						</div>
			} else if (hoveredWrapper.location === 'bottom') {
				hoveredWrapperBottom = <div style={{width: BlueprintWidths[width],
													height: '100%',
													position: 'absolute',
													top: 0,
													bottom: 0,
													pointerEvents: 'none'}}				
											className={'span-' + entity.get('width') + (hoveredWrapper.validDrop ? ' valid' : ' invalid')}>
										</div>
			} else if (hoveredWrapper.location === 'filler') {
			}
		}

		var wrapperClasses = 'span-' + width;
		wrapperClasses += hoveredWrapperBefore ? '' : ' prepend-' + prepend;
		wrapperClasses += hoveredWrapperAfter ? '' : ' append-' + append;
		wrapperClasses += last ? ' last' : '';
		
		if (hoveredWrapper && hoveredWrapper.location === 'self') {
			wrapperClasses += ' ' + (hoveredWrapper.validDrop ? 'valid' : 'invalid');
		}
	
		let id = entity.get('id');

		let style;
		if (hoveredWrapper && hoveredWrapper.location === 'self') {
			style = {
				//height: this.height
			};
		}
		
		var formEntityWrapper;

		if (entity.get('filler')) {
			formEntityWrapper = connectDropTarget(
				<div
					className={'last filler ' + 'span-' + entity.get('width') + ' ' + (hoveredWrapper ? (hoveredWrapper.validDrop ? 'valid' : 'invalid') : 'filler')}
					id={'filler'+ id}
					fillerStyle={fillerStyle}/>
			);
		} else if (entityType === EntityTypes.FORM_SECTION) {
			formEntityWrapper = connectDropTarget(
				<div
					id={'wrapper' + id}
					className={wrapperClasses}>
					{hoveredWrapperBefore}
					{ghostDiv}
					<FormSection
						{...this.props}/>
					{hoveredWrapperAfter}
					{hoveredWrapperBottom}
				</div>
			);
		} else if (entityType === EntityTypes.TEXT_INPUT) {
			formEntityWrapper = connectDropTarget(
				<div
					id={'wrapper' + id}
					className={wrapperClasses}
					style={style}>
					{hoveredWrapperBefore}
					{ghostDiv}
					<TextInput
						ref={(input) => this.entityRef = input}
						entity={this.props.entity}
						deleteFormEntity={this.props.deleteFormEntity}
						dropPrompt={this.props.dropPrompt}
						isValidDropPrompt={this.props.isValidDropPrompt}
						resizeFormEntity={this.props.resizeFormEntity}
						appState={this.props.appState}
						addingAssociationsSelected={this.state.addingAssociationsSelected}/>
					{hoveredWrapperAfter}
					{hoveredWrapperBottom}
				</div>
			);
		} else if (entityType === EntityTypes.TEXTAREA_INPUT) {
			formEntityWrapper = connectDropTarget(
				<div
					id={'wrapper' + id}
					className={wrapperClasses}>
					{hoveredWrapperBefore}
					{ghostDiv}
					<TextareaInput
						entity={this.props.entity}
						deleteFormEntity={this.props.deleteFormEntity}
						dropPrompt={this.props.dropPrompt}
						isValidDropPrompt={this.props.isValidDropPrompt}
						resizeFormEntity={this.props.resizeFormEntity}
						appState={this.props.appState}
						addingAssociationsSelected={this.state.addingAssociationsSelected}/>
					{hoveredWrapperAfter}
					{hoveredWrapperBottom}
				</div>
			);
		} else if (entityType === EntityTypes.SELECT_INPUT) {
			formEntityWrapper = connectDropTarget(
				<div
					id={'wrapper'+id}
					className={wrapperClasses}>
					{hoveredWrapperBefore}
					{ghostDiv}
					<SelectInput
						entity={this.props.entity}
						deleteFormEntity={this.props.deleteFormEntity}
						dropPrompt={this.props.dropPrompt}
						isValidDropPrompt={this.props.isValidDropPrompt}
						resizeFormEntity={this.props.resizeFormEntity}
						appState={this.props.appState}
						addingAssociationsSelected={this.state.addingAssociationsSelected}/>
					{hoveredWrapperAfter}
					{hoveredWrapperBottom}
				</div>
			);
		} else if (entityType === EntityTypes.CHECKBOX_INPUT) {
			formEntityWrapper = connectDropTarget(
				<div
					id={'wrapper'+id}
					className={wrapperClasses}>
					{hoveredWrapperBefore}
					{ghostDiv}
					<CheckboxInput
						entity={this.props.entity}
						deleteFormEntity={this.props.deleteFormEntity}
						dropPrompt={this.props.dropPrompt}
						isValidDropPrompt={this.props.isValidDropPrompt}
						resizeFormEntity={this.props.resizeFormEntity}
						appState={this.props.appState}
						addingAssociationsSelected={this.state.addingAssociationsSelected}/>
					{hoveredWrapperAfter}
					{hoveredWrapperBottom}
				</div>
			);
		} else if (entityType === EntityTypes.DATETIME_INPUT) {
			formEntityWrapper = connectDropTarget(
				<div
					id={'wrapper'+id}
					className={wrapperClasses}>
					{hoveredWrapperBefore}
					{ghostDiv}
					<DateTimeInput
						entity={this.props.entity}
						deleteFormEntity={this.props.deleteFormEntity}
						dropPrompt={this.props.dropPrompt}
						isValidDropPrompt={this.props.isValidDropPrompt}
						resizeFormEntity={this.props.resizeFormEntity}
						appState={this.props.appState}
						addingAssociationsSelected={this.state.addingAssociationsSelected}/>
					{hoveredWrapperAfter}
					{hoveredWrapperBottom}
				</div>
			);
		} else if (entityType === EntityTypes.TEXTBLOCK) { 
		formEntityWrapper = connectDropTarget(
			<div
				id={'wrapper'+id}
				className={wrapperClasses}>
				{hoveredWrapperBefore}
				{ghostDiv}
				<TextBlock
					entity={this.props.entity}
					deleteFormEntity={this.props.deleteFormEntity}
					resizeFormEntity={this.props.resizeFormEntity}
					appState={this.props.appState}/>
				{hoveredWrapperAfter}
				{hoveredWrapperBottom}
			</div>
		);

		} else if (entityType === EntityTypes.IMAGEBLOCK) {
			formEntityWrapper = connectDropTarget(
				<div
					id={'wrapper'+id}
					className={wrapperClasses}>
					{hoveredWrapperBefore}
					{ghostDiv}
					<ImageBlock
						entity={this.props.entity}
						deleteFormEntity={this.props.deleteFormEntity}
						resizeFormEntity={this.props.resizeFormEntity}
						appState={this.props.appState}/>
					{hoveredWrapperAfter}
					{hoveredWrapperBottom}
				</div>
			);
		} else if (entityType === EntityTypes.HIGHLIGHT_IMAGEBLOCK) {
			formEntityWrapper = connectDropTarget(
				<div
					id={'wrapper'+id}
					className={wrapperClasses}>
					{hoveredWrapperBefore}
					{ghostDiv}
					<HighlightImageBlock
						entity={this.props.entity}
						deleteFormEntity={this.props.deleteFormEntity}
						resizeFormEntity={this.props.resizeFormEntity}
						appState={this.props.appState}/>
					{hoveredWrapperAfter}
					{hoveredWrapperBottom}
				</div>
			);
		}
		return formEntityWrapper;
	}
});

var DropTarget = ReactDnD.DropTarget;
module.exports = {
	FormEntityWrapper: DropTarget([DnDTypes.TEXT_BOX, DnDTypes.PROMPT, NativeTypes.FILE], spec, collect)(FormEntityWrapper)
}