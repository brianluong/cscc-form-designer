const React = require('react');
const Radium = require('radium');


const FormDesignerActions = require('../../actions/FormDesignerActions.js');

const Constants = require('../../constants/Constants.js');
const InputTypes = Constants.INPUT_TYPES;
const ValidatorTypes = Constants.VALIDATOR_TYPES;

const ValidatorPropertiesCheckboxes = React.createClass({

	render: function() {

		return (
				<div>
					<input
						type='checkbox'
						checked={this.props.validState}
						onChange={(e) => FormDesignerActions.updateFormEntityProperty(this.props.entityId, this.props.keyPath.push('validState'), e.target.checked)}/>	
					Match

					<input
						type='checkbox'
						checked={this.props.nullIsValid}
						onChange={(e) => FormDesignerActions.updateFormEntityProperty(this.props.entityId, this.props.keyPath.push('nullIsValid'), e.target.checked)}/>	
					Allow Blank Values

					{this.props.appliedValidator ? 
						<span>
							<input
							type='checkbox'
							checked={this.props.strong}
							onChange={(e) => FormDesignerActions.updateFormEntityProperty(this.props.entityId, this.props.keyPath.push('strong'), e.target.checked)}/>	
							Overridable
						</span>
					: null}
				</div>
		);
	}

});

module.exports = {
	ValidatorPropertiesCheckboxes: ValidatorPropertiesCheckboxes
}