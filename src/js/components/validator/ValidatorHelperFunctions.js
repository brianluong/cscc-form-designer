
const _ = require('underscore');

const Constants = require('../../constants/Constants.js');
const InputTypes = Constants.INPUT_TYPES;
const ValidatorTypes = Constants.VALIDATOR_TYPES;

/**
 * Check when user changes Input Type, if currently configured validators are compatible
 * See: ValidatorEditor.jsx's validatorTypeMap constant	
 * @param {
 * @return {boolean} isCompleted
 */
function isValidatorCompatible(inputType, validatorType) {
	if (inputType === InputTypes.STRING) {
		if (validatorType === ValidatorTypes.RANGE) {
			return false;
		}
	} else if (inputType  === InputTypes.FLOAT || inputType === InputTypes.INTEGER) {
		if (validatorType === ValidatorTypes.SUBJECT_VALIDATION) {
			return false;
		}
	}

	if (validatorType === '') {
		return false;
	}
	return true;
}

/**
 * Checks if Validator is completed
 * @param {ImmutableJS Iterable} validator 
 * @return {boolean} isCompleted
 */
 // Todo: floats have to be floats, ints need to be ints
function isValidatorCompleted(validator) {
	const value = validator.get('properties').get('value');
	const validatorName = validator.get('properties').get('name');

	switch(validatorName) {
		case ValidatorTypes.PATTERN:
			return value.length > 0;
		case ValidatorTypes.EMPTY_FIELD: 
			return true;
		case ValidatorTypes.ENUM:
			return value.size > 0 && value.every((enumValue) => enumValue.length > 0);
		case ValidatorTypes.RANGE:
			return !isNaN(value.get('minValue')) 
				&& !isNaN(value.get('maxValue'))
				&& typeof parseFloat(value.get('minValue')) === 'number'
				&& typeof parseFloat(value.get('maxValue')) === 'number'
				&& ((value.get('minInclusive') && value.get('maxInclusive') && value.get('minValue') <= value.get('maxValue'))
					||  value.get('minValue') < value.get('maxValue'));
		case ValidatorTypes.SUBJECT_VALIDATION:
			return value != null;
		default:
			// Validator type has not been configured
			return false;
	}
}

// Applicable for EnumValidator, EmptyFieldValidator, RangeValidator, SubjectInputValidator
// Compute for validators with mutually exclusive conditions, 
// Ex. Match in Range 2-5, Match not in Range 2-5,
// I.e. No matter what values the user enters, set of validators will never pass
function computeMutuallyExclusiveValidators(validators, inputType) {

	let fieldMustBeLeftBlank = false;
	let fieldMustNotBeLeftBlank = false;
	let isAllValidatorsAllowBlankValues = true;

	let enumMustMatch = {};
	let enumCannotMatch = {};

	let rangeIn = [];
	let rangeNotIn = [];

	let enumCount = 0;
	let enumMustMatchCount = 0;
	let rangeCount = 0;

	for (let validatorIndex = 0; validatorIndex < validators.size; validatorIndex++) {
		let currentValidator = validators.get(validatorIndex);
		let currentValidatorType = currentValidator.get('properties').get('name');

		if ((!isValidatorCompatible(inputType, currentValidatorType))
			|| !isValidatorCompleted(currentValidator)) {
			continue;
		}

		if (currentValidatorType !== ValidatorTypes.EMPTY_FIELD
			&& !currentValidator.get('nullIsValid')) {
			// A validator that does not allow blank values
			fieldMustNotBeLeftBlank = true;
		}

		switch (currentValidatorType) {
			case ValidatorTypes.PATTERN:
				break;
			case ValidatorTypes.EMPTY_FIELD:
				if (currentValidator.get('properties').get('value')) {
					fieldMustBeLeftBlank = true;
				} else {
					fieldMustNotBeLeftBlank = true;
				}
				break;
			case ValidatorTypes.ENUM:
				if (currentValidator.get('validState')) {
					let uniqueValues = [];
					currentValidator.get('properties').get('value').map(
						(value) => {
							if (!uniqueValues.includes(value)) {
								enumMustMatch[value] = (enumMustMatch[value] || 0) + 1;
								uniqueValues.push(value);
							} 
						});
					enumMustMatchCount++;
				} else {
					currentValidator.get('properties').get('value').map(
						(value) => enumCannotMatch[value] = true);
				}
				break;
			case ValidatorTypes.RANGE:
				let minInclusive = currentValidator.get('properties').get('value').get('minInclusive');
				let maxInclusive = currentValidator.get('properties').get('value').get('maxInclusive');
				let minValue = parseInt(currentValidator.get('properties').get('value').get('minValue'));
				let maxValue = parseInt(currentValidator.get('properties').get('value').get('maxValue'));
				minValue = minInclusive ? minValue : minValue + 1;
				maxValue = maxInclusive ? maxValue : maxValue - 1;
				
				if (currentValidator.get('validState')) {
					rangeIn.push({
						minValue: minValue,
						maxValue: maxValue
					});
				} else {
					rangeNotIn.push({
						minValue: minValue,
						maxValue: maxValue
					});
				}
				rangeCount++;
				break;
			case ValidatorTypes.SUBJECT_VALIDATION:
				// TODO
				break;
		}
	}

	let warnings = [];
	if (fieldMustBeLeftBlank && fieldMustNotBeLeftBlank) {
		// Case 1: EmptyFieldValidator: must be blank && EmptyFieldValidator: must not be blank
		// Case 2: EmptyFieldValidator: must be blank && there exists another Validator: must not be blank (nullIsValid == false)
		warnings.push({message: 'Empty Field'});
	} else if (fieldMustNotBeLeftBlank) {
		// Case 1: EmptyFieldValidator: must not be blank
		// Case 2: At least one validator: must not be blank
		// Essentially treat every Validator without the option of being blank

		if (enumMustMatchCount) {
			let enumMatchIntersect = false;
			let intersectedEnumValues = [];
			for (let enumValue in enumMustMatch) {
				if (enumMustMatch[enumValue] === enumMustMatchCount) {
					enumMatchIntersect = true;
					intersectedEnumValues.push(enumValue)
				}
			}

			if (!enumMatchIntersect
				|| _.intersection(Object.keys(enumCannotMatch), intersectedEnumValues).length === intersectedEnumValues.length) {
				warnings.push({message: 'Enum'});
			}
		} else if (rangeCount) {
			let rangeIntersect = true;
			let intersectRange = {};

			// Find the intersection of ranges that value should fall in
			for (let i = 0; i < rangeIn.length; i++) {
				if (i === 0) {
					intersectRange.minValue = rangeIn[i].minValue;
					intersectRange.maxValue = rangeIn[i].maxValue;
				} else if (intersectRange.minValue > rangeIn[i].maxValue
							|| intersectRange.maxValue < rangeIn[i].minValue) {
					rangeIntersect = false;
					break;
				} else {
					intersectRange.minValue = rangeIn[i].minValue > intersectRange.minValue ? rangeIn[i].minValue : intersectRange.minValue;
					intersectRange.maxValue = rangeIn[i].maxValue < intersectRange.maxValue ? rangeIn[i].maxValue : intersectRange.maxValue;
				}
			}

			let intersectRangeMap = {};
			for (let i = intersectRange.minValue; i <= intersectRange.maxValue; i++) {
				intersectRangeMap[i] = true;
			}

			for (let notInRange of rangeNotIn) {
				for (let i = notInRange.minValue; i <= notInRange.maxValue; i++) {
					intersectRangeMap[i] = false;
				}
			}

			// let intsecq = [].push(intersectRange);
			// while (intsecq.length > 0) {
			// 	intsec = intsecq[0];
			// 	intsecq.shift();

			// 	for (let notInRange of rangeNotIn) {
			// 		if (notInRange.minValue < intsec.minValue
			// 			&& notInRange.maxValue < intsec.minValue) {
			// 			// nothing, no intersection
			// 		} else if (notInRange.minValue > intsect.maxValue) {
			// 			// nothing, no intersection
			// 		} else if (notInRange.minValue == intsec.minValue
			// 					&& notInRange.maxValue == intsec.maxValue) {
			// 			// nothing, full intersection
			// 		} else if (notInRange.minValue <= intsec.minValue
			// 					&& notInRange.maxValue <= intsec.maxValue) {
			// 			if (notInRange.maxValue !=== intsec.maxValue) {
			// 				intsecq.push({minValue: notInRange.maxValue, maxValue: intsec.maxValue});
			// 			}
			// 		} else if (notInRange.minValue >= intsec.minValue 
			// 					&& notInRange.maxValue >= intsec.maxValue) {
			// 			if (intsec.minValue !== notInRange.minValue) {
			// 				intsecq.push({minValue: intsec.minValue, maxValue: notInRange.minValue});
			// 			}
			// 		} else if (notInRange.minValue > intsec.minValue
			// 					&& notInRange.maxValue < intsec.maxValue) {
			// 			intsecq.push({minValue: intsec.minValue, maxValue: notInRange.minValue});
			// 			intsecq.push({minValue: notInRange.maxValue, maxValue: intsec.maxValue});
			// 		}
			// 	}
			// }


			
			// for (let notInRange of rangeNotIn) {
			// 	if (notInRange.minValue <= intersectRange.minValue
			// 		&& notInRange.maxValue >= intersectRange.maxValue) {

			// 	}

			// 	if (notInRange.minValue >= intersectRange.minValue
			// 		&& notInRange.minValue <= intersectRange.maxValue) {
			// 		if (notInRange.maxValue <= intersectRange.maxValue) {
			// 			intersectRange.minValue = notInRange.minValue;
			// 			intersectRange.maxValue = notInRange.maxValue;
			// 		} else {
			// 			intersectRange.minValue = notInRange.minValue;
			// 		}
			// 	} else if (notInRange.minValue < intersectRange.minValue
			// 				&& notInRange.maxValue > intersectRange.minValue) {
			// 		if (notInRange.maxValue >= intersectRange.minValue
			// 			&& notInRange.maxValue <= intersectRange.maxValue) {
			// 			intersectRange.maxValue = notInRange.maxValue;
			// 		} else {

			// 		}
			// 	}
			// 	intersectRange.minValue = notInRange.minValue > intersectRange.minValue ? rangeIn[i].minValue : intersectRange.minValue;
			// 	intersectRange.maxValue = notInRange.maxValue < intersectRange.maxValue ? rangeIn[i].maxValue : intersectRange.maxValue;
			// }

			if (!rangeIntersect
				|| (Object.keys(intersectRange).length > 0 
					&& Object.keys(intersectRangeMap).every((key) => !intersectRangeMap[key]))) {
				warnings.push({message: 'Range'});
			}
		}
	
	} else {
		// Blank inputs are considered valid
	}
	return warnings;
}


module.exports = {
	isValidatorCompleted: isValidatorCompleted,
	isValidatorCompatible: isValidatorCompatible,
	computeMutuallyExclusiveValidators: computeMutuallyExclusiveValidators
}