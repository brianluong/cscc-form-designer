const React = require('react');

const FormDesignerActions = require('../../actions/FormDesignerActions.js');

const PatternValidatorEditor = React.createClass({
	
	updatePattern: function(e) {
		FormDesignerActions.updateFormEntityProperty(this.props.entityId, this.props.keyPath.concat(['properties', 'value']), e.target.value);
	},

	render: function() {
		const patternValidatorEditorStyle = {
			display: 'inline-block',
			marginLeft: '5px'
		};

		const patternValidatorTextInputStyle = {
			width: '70px'
		};

		return (
			<div
				style={patternValidatorEditorStyle}>
				Pattern:  
				<input
					type='text'
					style={patternValidatorTextInputStyle}
					value={this.props.pattern}
					onChange={this.updatePattern}/>
			</div>
		);
	}
});

module.exports = {
	PatternValidatorEditor: PatternValidatorEditor
}