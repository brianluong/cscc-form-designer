const React = require('react');
const ReactDnD = require('react-dnd');
const Radium = require('radium');
const Immutable = require('immutable');
const _ = require('underscore');
const Collapse = require('rc-collapse');
const Panel = Collapse.Panel;

const FormDesignerActions = require('../../actions/FormDesignerActions.js');

const Constants = require('../../constants/Constants.js');
const InputTypes = Constants.INPUT_TYPES;
const ValidatorTypes = Constants.VALIDATOR_TYPES;
const FocusOptions = Constants.FOCUS_OPTIONS;

const ValidatorHelperFunctions = require('./ValidatorHelperFunctions.js');
const ValidatorPanel = require('./ValidatorPanel.jsx').ValidatorPanel;
const ValidatorEditor = require('./ValidatorEditor.jsx').ValidatorEditor;

const Draggable = require('react-draggable');
var ResizableBox = require('react-resizable').ResizableBox;
const Portal = require('react-portal');

const validatorBaseKeyPath = Immutable.fromJS(['validators']);

let ValidatorsBox = Radium(React.createClass({
	getInitialState: function() {
		return {
			activeKey: undefined,
			warningMessages: [],
			width: 300,
			height: 280
		}
	},

	componentWillMount: function() {
		this.setState({
			warningMessages: ValidatorHelperFunctions.computeMutuallyExclusiveValidators(this.props.entity.get('validators'), this.props.entity.get('inputType'))
		});
	},

	componentDidMount: function() {
		let formEntityAttributesBoxDimensions = document.getElementById('formEntityAttributesBox').getBoundingClientRect();
		this.setState({
			// Rendering on the right of FormEntityAttributesBox
			top: this.props.appState.get('validatorsBoxAttributes').get('coords') ? 
				this.props.appState.get('validatorsBoxAttributes').get('coords').get('top') : formEntityAttributesBoxDimensions.top,
			left: this.props.appState.get('validatorsBoxAttributes').get('coords') ? 
				this.props.appState.get('validatorsBoxAttributes').get('coords').get('left') : formEntityAttributesBoxDimensions.right,
			width: this.props.appState.get('validatorsBoxAttributes').get('dimensions') ? 
				this.props.appState.get('validatorsBoxAttributes').get('dimensions').get('width') : this.state.width,
			height: this.props.appState.get('validatorsBoxAttributes').get('dimensions') ? 
				this.props.appState.get('validatorsBoxAttributes').get('dimensions').get('height') : this.state.height,
			headerHeight: this.calculateHeaderHeight()
		});
		FormDesignerActions.setWindowFocus(FocusOptions.VALIDATORS);
	},

	componentWillReceiveProps: function(nextProps) {
		this.setState({
			warningMessages: ValidatorHelperFunctions.computeMutuallyExclusiveValidators(nextProps.entity.get('validators'), nextProps.entity.get('inputType'))
		});
	},

	componentDidUpdate: function() {
		window.setTimeout(
			() => {
				let newHeaderHeight = this.calculateHeaderHeight();
				if (newHeaderHeight !== this.state.headerHeight) {
					this.setState({
						headerHeight: newHeaderHeight
					});
				}
			}
		, 0); 
		// Weird, but if setTimeout isn't used, calculateHeaderHeight() returns wrong value
		// Seems like componentDidUpdate fires before all the browser DOM manipulations are complete
	},

	calculateHeaderHeight: function() {
		return document.getElementById('validatorBoxHeader').getBoundingClientRect().height;
	},

	deleteValidator: function(index) {
		return (e) => {
			FormDesignerActions.updateFormEntity(this.props.entity.get('id'), 
				{
					validators: this.props.entity.get('validators').delete(index)
				});
			e.stopPropagation();
			if (this.state.activeKey) {
				if (index < this.state.activeKey.match(/\w+/)) {
					this.setState({
						activeKey: this.state.activeKey.replace(/\w+/, this.state.activeKey.match(/\w+/)[0] - 1)
					});
					// Using == instead of === because comparing number to 'number'
				} else if (index == this.state.activeKey.match(/\w+/)) {
					this.setState({
						activeKey: undefined
					});
				}
			}
		}
	},

	renderValidators: function() {
		return this.props.entity.get('validators').map(
			(value, index) => {
				let compatible = ValidatorHelperFunctions.isValidatorCompatible(
									this.props.entity.get('inputType'), 
									this.props.entity.get('validators').get(index).get('properties').get('name'));
				return (
					<Panel
						header={<ValidatorPanel
									validator={this.props.entity.get('validators').get(index)}
									compatible={compatible}
									inputType={this.props.entity.get('inputType')}
									deleteValidator={this.deleteValidator(index)}
									appliedValidator/>}
						key={index}
						index={index}>
						<ValidatorEditor
							validator={this.props.entity.get('validators').get(index)}
							compatible={compatible}
							inputType={this.props.entity.get('inputType')}
							entityId={this.props.entity.get('id')}
							keyPath={validatorBaseKeyPath.push(index)}
							appliedValidator/>
					</Panel>)}
		).toJS();
	},

	addValidator: function(e) {
		FormDesignerActions.updateFormEntity(this.props.entity.get('id'), 
			{
				validators: this.props.entity.get('validators').push(
					Immutable.fromJS(
						{
							properties: {
								name: '',
								value: ''
							},
							validState: true,
							strong: false,
							nullIsValid: false,
							customFailureMessages: []
						}
					))
			}
		)
	},

	toggleAccordion: function(activeKey) {
		this.setState({
			activeKey: activeKey
		});
	},

	onResize: function(event, dimensions) {
		let newHeaderHeight = document.getElementById('validatorBoxHandle').getBoundingClientRect().height;
		if (newHeaderHeight !== this.state.headerHeight) {
			this.setState({
				headerHeight: newHeaderHeight
			});
		}
	},

	focusValidatorsBox: function(e) {
		e.stopPropagation();
		FormDesignerActions.setWindowFocus(FocusOptions.VALIDATORS);
	},

	setValidatorsBoxCoords: function(e, data) {
		FormDesignerActions.setWindowAttributes(FocusOptions.VALIDATORS, 
			'coords',
			Immutable.fromJS({
				top: data.node.getBoundingClientRect().top,
				left: data.node.getBoundingClientRect().left
			}));
	},

	// setTimeout on onResizeStop is a little hack to delay enabling canDrag in appState so 
	// that the conditionals in the  'click' event in Main.js won't pass
	onResizeStop: function(e, dimensions) {
		window.setTimeout(() => FormDesignerActions.enableDrag(), 0);
		FormDesignerActions.setWindowAttributes(FocusOptions.VALIDATORS, 
			'dimensions', 
			Immutable.fromJS({
				width: dimensions.size.width,
				height: dimensions.size.height
			}));
	},

	render: function() {
		const entity = this.props.entity;

		const validatorsContainerStyle = {
			width: this.state.width, 
			height: this.state.height,
			marginBottom: '10px',
			border: '1px solid black',
			background: 'rgb(227,220,192)',
			clear: 'both',
			'@media (max-width: 1527px)' : {
				float: 'left',
				clear: 'none'
			},
			position: 'absolute',
			top: this.state.top,
			left: this.state.left,
			zIndex: this.props.appState.get('windowFocus') === FocusOptions.VALIDATORS ? 99999 : 'auto'
		};

		const validatorListStyle = {
			background: 'white',
			position: 'fixed',
			left: 0,
			right: 0,
			top: this.state.headerHeight || '25px',
			bottom: '20px',
			overflow: 'auto'
		};

		const validatorHeaderStyle = {
			position: 'fixed', 
			top: 0, 
			left: 0, 
			right: 0
		};

		const headerMessageStyle = {
			background: this.props.appState.get('windowFocus') === FocusOptions.VALIDATORS ? 'green': 'rgba(0,0,255,.3)',
			':hover': {
				cursor: 'pointer'
			}
		};

		const warningMessageStyle =  {
			background: 'rgba(255,0,0,.3',
		};

		const validatorBoxFooterStyle = {
			position: 'fixed', 
			bottom: 0, 
			left: 0,
			right: 0,
			width: '100%', 
			height: '20px'
		};

		const addButtonStyle = {
			'backgroundImage': 'url(statics/plus-button.jpeg)', 
			'backgroundSize': 'contain', 
			'backgroundRepeat': 'no-repeat', 
			'height': '15px',
		};

		// setTimeout on onResizeStop is a little hack to delay enabling canDrag in appState so 
		// that the conditionals in the  'click' event in Main.js won't pass
		return (
			// Using Portal to attach on HTML body element 
			// Useful so dimensions won't affect the layout of other HTML elements
			<Portal
				isOpened>
				<Draggable
					handle='#validatorBoxHandle'
					onMouseDown={this.focusValidatorsBox}
					onStop={this.setValidatorsBoxCoords}>
					<div
						style={validatorsContainerStyle}
						onClick={this.focusValidatorsBox}>
						<ResizableBox
							width={this.state.width}
							height={this.state.height}
							minConstraints={[0, this.state.headerHeight + 20]}
							onResize={(e, dimensions) => this.setState({width: dimensions.size.width, height: dimensions.size.height})}
							onResizeStart={(e) => FormDesignerActions.disableDrag()}
							onResizeStop={(e) => window.setTimeout(() => FormDesignerActions.enableDrag(), 0)}
							onResizeStop={this.onResizeStop}>

							<div
								id='validatorBoxHeader'
								style={validatorHeaderStyle}>
								<div
									style={headerMessageStyle}
									id={'validatorBoxHandle'}>
										Validators for {entity.get('entityType') + entity.get('id')} of 
										Input Type {entity.get('inputType')}
								</div>
								
								{this.state.warningMessages.length > 0 ? 
									<div
										style={warningMessageStyle}>
											WARNING With this current combination of validators, the input will never be considered valid.
											Check the {this.state.warningMessages[0].message} Validator
									</div>
								: null}
							</div>

							<div
								style={validatorListStyle}
								id={'validatorsContainer'}>
								<Collapse
									activeKey={this.state.activeKey}
									onChange={this.toggleAccordion}
									accordion>
									{this.renderValidators()}
								</Collapse>
							</div>

							<div
								style={validatorBoxFooterStyle}>
								<button
									onClick={this.addValidator}
									style={addButtonStyle}/>
							</div>
						</ResizableBox>
					</div>
				</Draggable>
			</Portal>
		);
	}
}));

const validatorBoxDragSourceSpec = {
	beginDrag: function() {
		return {

		}
	}
};

const validatorBoxDragSourceCollect = function(connect, monitor) {
	return {
		connectDropTarget: connect.dropTarget(),
		isDragging: monitor.isDragging()
	}
};

// const DragSource = ReactDnD.DragSource;
// ValidatorsBox = DragSource(DnDTypes.VALIDATOR_BOX, validatorBoxDragSourceSpec, validatorBoxDragSourceCollect)(ValidatorsBox);

module.exports = {
	ValidatorsBox: ValidatorsBox
}