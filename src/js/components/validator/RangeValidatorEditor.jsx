const React = require('react');
const Radium = require('radium');

const FormDesignerActions = require('../../actions/FormDesignerActions.js');

const RangeValidatorEditor = Radium(React.createClass({

	onChangeMinInclusive: function(e) {
		FormDesignerActions.updateFormEntityProperty(
			this.props.entityId,
			this.props.keyPath.concat(['properties', 'value', 'minInclusive']),
			e.target.checked);
	},

	onChangeMaxInclusive: function(e) {
		FormDesignerActions.updateFormEntityProperty(
			this.props.entityId,
			this.props.keyPath.concat(['properties', 'value', 'maxInclusive']),
			e.target.checked);	
	},

	onChangeMinValue: function(e) {
		FormDesignerActions.updateFormEntityProperty(
			this.props.entityId,
			this.props.keyPath.concat(['properties', 'value', 'minValue']),
			e.target.value);	
	},

	onChangeMaxValue: function(e) {
		FormDesignerActions.updateFormEntityProperty(
			this.props.entityId,
			this.props.keyPath.concat(['properties', 'value', 'maxValue']),
			e.target.value);		
	},

	// Expected rangeProperties
	render: function() {
		const rangeValidatorEditorStyle = {
			display: 'inline-block',
			marginLeft: '5px',
			marginTop: '5px'
		}

		const inputValuesStyle = {
			width: '30px',
			margin: '2px'
		};

		const lessThanButton1Style = {
			background: this.props.rangeProperties.get('minInclusive') 
				? 'url(statics/less-than-equal.png) 0% 0% / contain' : 'url(statics/less-than.png) 0% 0% / contain',
			width: '15px',
			border: 'none',
			height: '15px',
			':hover' : {
				cursor: 'pointer'
			}
		}

		const lessThanButton2Style = {
			background: this.props.rangeProperties.get('maxInclusive') 
				? 'url(statics/less-than-equal.png) 0% 0% / contain' : 'url(statics/less-than.png) 0% 0% / contain',
			border: 'none',
			width: '15px',
			height: '15px',
			':hover' : {
				cursor: 'pointer'
			}
		}

		// Need Validation = only numbers allowed 
		return (
			<div
				style={rangeValidatorEditorStyle}>
				<div>
					Min:
					<input
						style={inputValuesStyle}
						type='text'
						onChange={this.onChangeMinValue}
						value={this.props.rangeProperties.get('minValue')}/>
					 Inclusive 
					<input
						onChange={this.onChangeMinInclusive}
						checked={this.props.rangeProperties.get('minInclusive')}
						type='checkbox'/>
				</div>
				<div>
					Max:
					<input
						style={inputValuesStyle}
						type='text'
						onChange={this.onChangeMaxValue}
						value={this.props.rangeProperties.get('maxValue')}/>
					Inclusive
					<input
						onChange={this.onChangeMaxInclusive}
						checked={this.props.rangeProperties.get('maxInclusive')}
						type='checkbox'/>
				</div>
				
			</div>
		);
	}
}));


module.exports = {
	RangeValidatorEditor: RangeValidatorEditor
}