const React = require('react');

const Constants = require('../../constants/Constants.js');
const InputTypes = Constants.INPUT_TYPES;
const ValidatorTypes = Constants.VALIDATOR_TYPES;

const ValidatorHelperFunctions = require('./ValidatorHelperFunctions.js');

// Max width until the <span> carries over into two lines

const ValidatorPanel = React.createClass({
	getInitialState: function() {
		return {
			overflow: false,
			shortenedValue: ''
		};
	},

	// Only PatternValidator and EnumValidator can overflow
	// Repeatedly filling a temporary <span> and with 
	// descreasingly shorter string representations until it fits
	shorten: function(validatorType) {
		let value = this.props.validator.get('properties').get('value');
		let span = document.createElement('span');
		span.style.whiteSpace = 'nowrap';
	    let spanInserted = document.getElementById('validatorsContainer').appendChild(span);
	    
	    let shortenedValue;
	    if (validatorType === ValidatorTypes.PATTERN) {
	    	shortenedValue = value;
	    	spanInserted.textContent = this.generatePatternString(this.props.validator.get('validState'), shortenedValue);
		} else if (validatorType === ValidatorTypes.ENUM) {
			shortenedValue = this.generateEnumStringRepresentation(value);
			shortenedValue = shortenedValue.substr(0, shortenedValue.length-1) + ',...}';
			let textContent = this.generateEnumString(this.props.validator.get('validState'), shortenedValue);
			spanInserted.textContent = textContent.substr(0, textContent.length-1);
		}
	    let spanInsertedWidth = spanInserted.getBoundingClientRect().width;

	   	const MAX_HEADER_LENGTH = document.getElementById('validatorPanel').getBoundingClientRect().width - 20;

	    while (spanInsertedWidth > MAX_HEADER_LENGTH) {
	    	if (validatorType === ValidatorTypes.PATTERN) {
		    	shortenedValue = shortenedValue.substring(0, value.length-4) + '...';
				spanInserted.textContent = this.generatePatternString(this.props.validator.get('validState'), shortenedValue);
	    	} else if (validatorType === ValidatorTypes.ENUM) {
	    		shortenedValue = shortenedValue.replace(/(^{)(.*,)*(.*,)(\s*...})$/, '$1$2$4');
	    		spanInserted.textContent = this.generateEnumString(this.props.validator.get('validState'), shortenedValue);
	    	}
	    	spanInsertedWidth = spanInserted.getBoundingClientRect().width;
	    }

		document.getElementById('validatorsContainer').removeChild(spanInserted);
	    return shortenedValue;
	},

	// componentWillReceiveProps: function(nextProps) {
	// 	let value = nextProps.validator.get('properties').get('value');
 //    	let span = document.createElement('span');
 //    	span.style.whiteSpace = 'nowrap';
	//     let spanInserted = document.getElementById('validatorsContainer').appendChild(span);

	//     const validatorName = this.props.validator.get('properties').get('name');
	//     if (validatorName === ValidatorTypes.PATTERN) {
	//     	spanInserted.textContent = this.generatePatternString(nextProps.validator.get('validState'), value);
	// 	} else if (validatorName === ValidatorTypes.ENUM) {
	// 		spanInserted.textContent = this.generateEnumString(nextProps.validator.get('validState'), value);
	// 	}
	//     let spanInsertedWidth = spanInserted.getBoundingClientRect().width;
	//    	document.getElementById('validatorsContainer').removeChild(spanInserted);

	//    	const MAX_HEADER_LENGTH = document.getElementById('validatorPanel').getBoundingClientRect().width - 20;
	//    	console.log( MAX_HEADER_LENGTH)
	//    	console.log('span inserted width ' + spanInsertedWidth)
	    
	//     if (spanInsertedWidth > MAX_HEADER_LENGTH) {
	//     	if (!this.state.overflow
	//     		|| value !== this.props.validator.get('properties').get('value')) {
	//     		this.setState({
	//     			overflow: true,
	//     			shortenedValue: this.shorten(nextProps.validator.get('properties').get('name'))
	//     		});
	//     	}
	//     } else {
	//     	if (this.state.overflow) {
	//     		this.setState({
	//     			overflow: false,
	//     			shortenedValue: ''
	//     		});
	//     	}
	//     }
	// },

	generatePatternString: function(matching, pattern) {
		return 'Field must ' + (matching ? 'match ' : 'not match ') + (pattern.length > 0 ? '\'' + pattern  + '\'': 'N/A');
	},

	generateEnumString: function(matching, enumValues) {
		return 'Field must ' + (matching ? '' : 'not') + ' be one of ' 
			+ (typeof enumValues === "string" ? enumValues : this.generateEnumStringRepresentation(enumValues));
	},

	generateEnumStringRepresentation: function(enumValues) {
		return enumValues.reduce((reduction, value, index, enumValues) => reduction.substr(0, reduction.length-1) + value + (index < enumValues.size -1 ? ', ' : '}'), '{}');
	},

	generateRangeString: function(matching, rangeProps) {
		return 'Field '
				+ (matching ? '' : 'not')
				+ ' in range ['
				+ (rangeProps.get('minValue') === '' ? 'N/A' : rangeProps.get('minValue')) + ', '
				+ (rangeProps.get('maxValue') === '' ? 'N/A' : rangeProps.get('maxValue'))
				+ '] '
				+ (ValidatorHelperFunctions.isValidatorCompleted(this.props.validator) 
					? ((rangeProps.get('minInclusive') ? ' Incl' : ' Excl')
						+ ', '
						+ (rangeProps.get('maxInclusive') ? ' Incl' : ' Excl'))
					: '');
	},

	// Todo
	generateSubjectInputValidatorString: function(matching, value) {
		return 'Subject Input Validator : ' + value;
	},

	render: function() {

		const validator = this.props.validator;
		const validatorType = validator.get('properties').get('name');
		const matching = validator.get('validState');
		const value = validator.get('properties').get('value');
		let headerString = '';

		switch(validatorType) {
			case ValidatorTypes.PATTERN:
				let pattern = value;
				if (this.state.overflow) {
					pattern = this.state.shortenedValue;
				}
				headerString = this.generatePatternString(matching, pattern);
				break;
			case ValidatorTypes.EMPTY_FIELD: 
				headerString = 'Field must ' + (value ? '' : 'not') + ' be left blank';
				break;
			case ValidatorTypes.ENUM:
				let enums = value;
				if (this.state.overflow) {
					enums = this.state.shortenedValue;
				}
				headerString = this.generateEnumString(matching, enums);
				break;
			case ValidatorTypes.RANGE:
				// Todo overflow for range
				headerString = this.generateRangeString(matching, value);
				break;
			case ValidatorTypes.SUBJECT_VALIDATION:
				headerString = this.generateSubjectInputValidatorString(matching, value);
				break;
			default: 
				break;
		}

		let extraPropsString = '';
		switch(validatorType) {
			case ValidatorTypes.PATTERN:
			case ValidatorTypes.ENUM:
			case ValidatorTypes.RANGE:
			case ValidatorTypes.SUBJECT_VALIDATION:
				extraPropsString = (validator.get('nullIsValid') ? 'Can Leave Blank' : 'Can\'t Leave Blank')
									 + (this.props.appliedValidator ? (', '  + (validator.get('strong') ? 'Non-Overridable' : 'Overridable')) : '');
 				break;
			case ValidatorTypes.EMPTY_FIELD: 
				break;
			default: 
				break;
		}	

		// Boolean flag kind of a hack to style line-height
		let isIncompatible = false;
		// For ValidatorConditions that have been deleted
		if (!this.props.appliedValidator && this.props.validator.get('deleted')) {
			headerString = 'This Form Input has been deleted';
			extraPropsString = '';
		} else if (!this.props.compatible) {
			if (validatorType === '') {
				headerString = 'Validator Unconfigured';
			} else {
				headerString = validatorType.charAt(0).toUpperCase() + validatorType.slice(1) + ' validator incompatible with '
								+ 'Input Type: ' + this.props.inputType; //for type ' + this.props.inputType;
				isIncompatible = true;
			}
			extraPropsString = '';
		}

		const validatorPanelStyle = {
			display: 'inline-block',
			position: 'relative',
			textIndent: '0px',
			width: 'calc(100% - 30px)', // hack because CSS sucks
			background: ValidatorHelperFunctions.isValidatorCompleted(this.props.validator) && this.props.compatible ? 'rgba(0,255,0,.3)' : 'rgba(255,0,0,.3)'
		};

		const deleteValidatorButtonStyle = {
			backgroundImage: 'url(statics/x-button.png)', 
			backgroundSize: 'contain', 
			backgroundRepeat: 'no-repeat', 
			height: '15px', 
			width: '15px',
			verticalAlign: 'middle'
		};

		// 19px because it's half of the inherited line height (defined in Panel styles)
		let headerStringStyle = {
			lineHeight: extraPropsString.length > 0 ? '19px' : 'inherit',
			margin: '0px'
		};

		let extraPropsStyle = {
			lineHeight: '19px', 
			margin: '0px'
		};

		// Because this string is particularly long, setting the lineHeight to 19px to accomodate two lines
		if (isIncompatible) {
			headerStringStyle.lineHeight = '19px';
		}

		return (
			<div
				style={validatorPanelStyle}
				id={'validatorPanel'}
				onClick={(e) => e.stopPropagation()}>
				<div
					style={{float: 'left'}}>
					<p
						style={headerStringStyle}>
						{headerString}
					</p>
					{extraPropsString.length > 0 ? 
						<p
							style={extraPropsStyle}>
							{extraPropsString}
						</p> 
					: null}
				</div>
				<div
					style={{float: 'right'}}>
					<button
						onClick={this.props.deleteValidator}
						style={deleteValidatorButtonStyle}/>
				</div>
			</div>

		);
	}
});

module.exports = {
	ValidatorPanel : ValidatorPanel
}