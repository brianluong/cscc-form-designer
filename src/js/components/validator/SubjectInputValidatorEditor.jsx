const React = require('react');

const SubjectInputValidatorEditor = React.createClass({
	render: function() {
		const subjectInputValidatorEditorStyle = {
			display: 'inline-block',
			marginLeft: '5px'
		}
		return (
			<div
				style={subjectInputValidatorEditorStyle}>
				<select/>
			</div>
		);
	}
});

module.exports = {
	SubjectInputValidatorEditor: SubjectInputValidatorEditor
}