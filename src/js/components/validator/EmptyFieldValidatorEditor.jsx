const React = require('react');

const FormDesignerActions = require('../../actions/FormDesignerActions.js');

const EmptyFieldValidatorEditor = React.createClass({

	toggleChecked: function(e) {
		FormDesignerActions.updateFormEntityProperty(this.props.entityId, this.props.keyPath.concat(['properties', 'value']), e.target.checked);
	},

	render: function() {
		const emptyFieldValidatorEditorStyle = {
			display: 'inline-block',
			margin: '0.5em 0'
		};

		return (
			<div
				style={emptyFieldValidatorEditorStyle}>
				<input
					type='checkbox'
					checked={this.props.checked}
					onChange={this.toggleChecked}/>
				Field is blank
			</div>
		);
	}
});


module.exports = {
	EmptyFieldValidatorEditor: EmptyFieldValidatorEditor
}