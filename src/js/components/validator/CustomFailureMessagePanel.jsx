const React = require('react');

const Constants = require('../../constants/Constants.js');
const InputTypes = Constants.INPUT_TYPES;
const ValidatorTypes = Constants.VALIDATOR_TYPES;

const CustomFailureMessagePanel = React.createClass({
	isCompleted: function() {
		const message = this.props.customFailureMessage.get('message');
		const language = this.props.customFailureMessage.get('language');
		return message && language;
	},

	render: function() {

		const completed = this.isCompleted();

		const customFailureMessagePanelStyle = {
			display: 'inline-block',
			position: 'relative',
			width: '187px',
			textIndent: '0px',
			background: completed ? '' : 'rgba(255,0,0,.3)'
		};

		const deleteValidatorButtonStyle = {
			backgroundImage: 'url(statics/x-button.png)', 
			backgroundSize: 'contain', 
			backgroundRepeat: 'no-repeat', 
			height: '15px', 
			width: '15px',
			verticalAlign: 'middle'
		};

		let headerString;
		if (!completed) {
			headerString = 'Failure Message not configured'
		} else {
			headerString = 'Failure Message ';
		}
		
		return (
			<div
				style={customFailureMessagePanelStyle}>
				<span>
					{headerString}
				</span>
				<div
					style={{float: 'right'}}>
					<button
						onClick={this.props.deleteCustomFailureMessage}
						style={deleteValidatorButtonStyle}/>
				</div>
			</div>

		);
	}
});

module.exports = {
	CustomFailureMessagePanel: CustomFailureMessagePanel
}