var React = require('react');
var ReactDnD = require('react-dnd');
var getEmptyImage = require('react-dnd-html5-backend').getEmptyImage;
var PureRenderMixin = require('react-addons-pure-render-mixin');
var Radium = require('radium');	

var FormDesignerActions = require('../actions/FormDesignerActions.js');
var AppStateStore = require('../stores/AppStateStore.jsx');

var BlueprintWidths = require('../constants/blueprint-widths.js');
var Constants = require('../constants/Constants.js');
var EntityTypes = Constants.ENTITY_TYPES;
var DnDTypes = Constants.DND_TYPES;

const FormEntityHOC = function(Component) {
	const FormEntity = Radium(React.createClass({

		componentDidMount: function() {
    		this.props.connectDragPreview(getEmptyImage());
		},

		// Does this work?
		shouldComponentUpdate: function(nextProps, nextState) {
			return PureRenderMixin.shouldComponentUpdate(nextProps, nextState);
		},

		setSelectedEntity: function(e) {
			e.stopPropagation();
			FormDesignerActions.setSelectedEntity(this.props.entity.get('id'));
		},

		toggleAssociation: function(e) {
			e.stopPropagation();
			if (this.props.entity.get('entityType') === EntityTypes.TEXT_INPUT
				|| this.props.entity.get('entityType') === EntityTypes.TEXTAREA_INPUT
				|| this.props.entity.get('entityType') === EntityTypes.CHECKBOX_INPUT) {
				FormDesignerActions.toggleAssociation(this.props.entity.get('id'));
			}
		},

		onResize: function(event, dimensions) {
			var newWidth = Math.ceil(dimensions.size.width / Constants.PIXEL_TO_WIDTH_CONSTANT);
			this.props.resizeFormEntity(this.props.entity.get('id'), newWidth, 'width');
			this.forceUpdate(); // So that invalid states will reset (render) to valid state even while mouse is still dragging
		},

		onResizeStart: function(event) {
			FormDesignerActions.disableDrag();
			FormDesignerActions.setSelectedEntity(this.props.entity.get('id'));
		},

		onResizeStop: function(event) {
			FormDesignerActions.enableDrag();
		},

		render: function() {
			const connectDragSource = this.props.connectDragSource;
			const connectDropTarget = this.props.connectDropTarget;
			const connectDragPreview = this.props.connectDragPreview;
			const entity = this.props.entity;
			
			let id = entity.get('id');
			let width = entity.get('width');
			let entityType = entity.get('entityType');
			let promptPreWidth = entity.get('promptPreWidth') || 0;
			let promptPreText = entity.get('promptPreText');
			let promptPostWidth = entity.get('promptPostWidth') || 0;
			let promptPostText = entity.get('promptPostText');

			let formInputStyle = {
				opacity: this.props.isDragging ? 0 : 1,
				marginRight: 0,
				background: this.props.appState.get('selectedEntityId') === id ?  'green' : 'rgba(204,158,228,.5)',
				position: 'relative',
				cursor: 'move',
				boxSizing: 'border-box'
			};

			if (this.props.isDragging) {
				//formInputStyle.height = '100%'
			}

			// if (this.props.isDragging && !this.props.isOver) {
			// 	formInputStyle.height = '41px';
			// 	formInputStyle.overflow = 'hidden';
			// }

			let hoveredArea = null;
			if (this.props.isOver) {
				var hoveredAreaStyle = {
					width: BlueprintWidths[width + promptPreWidth + promptPostWidth],
					height: '100%',
					width: '100%',
					position: 'absolute',
					zIndex: 1,
					pointerEvents:'none'
				};

				hoveredArea = <div 
								style={hoveredAreaStyle}
								className={'invalid'}/>;
			}

			let addingAssociationsLayoverStyle = {
				top: 0,
				left: 0,
				height: '100%',
				width: '100%',
				background: this.props.addingAssociationsSelected ? 'green' : 'red',
				position: 'absolute',
				opacity: '.6',
				':hover': {
					cursor: 'pointer'
				}
			};

			if (this.props.appState.get('addingAssociations')
				&& this.props.appState.get('selectedEntityId') === id) {
				addingAssociationsLayoverStyle.background = 'blue';
			}

			let addingAssociationsLayover = null;
			if (this.props.appState.get('addingAssociations')) {
				addingAssociationsLayover = 
					<div
						key={'uniqueKeyForRadium'}
						style={addingAssociationsLayoverStyle}
						onClick={this.toggleAssociation}/>;
			}

			let className = 'span-' + (width + promptPreWidth + promptPostWidth);

			return connectDragSource(connectDropTarget(
				<div
					id={entityType + id}
					className={className}
					style={formInputStyle}
					onClick={this.setSelectedEntity}>
					<Component 
						{...this.props}
						{...this.state}
						onResize={this.onResize}
						onResizeStart={this.onResizeStart}
						onResizeStop={this.onResizeStop}/>
					{addingAssociationsLayover}
					{hoveredArea}
				</div>
			));
		}
	}));

	var formEntityDragSourceSpec = {
		beginDrag: function(props, monitor, component) {
			FormDesignerActions.setSelectedEntity(props.entity.get('id'));

			let entity = props.entity;
			let item = {
				id: entity.get('id'),
				width: entity.get('width'), 
				prepend: entity.get('prepend'),
				append: entity.get('append'),
				last: entity.get('last'),
				entityType: entity.get('entityType')
			};

			if (entity.get('entityType') === EntityTypes.TEXT_INPUT
				|| entity.get('entityType') === EntityTypes.SELECT_INPUT
				|| entity.get('entityType') === EntityTypes.CHECKBOX_INPUT
				|| entity.get('entityType') === EntityTypes.TEXTAREA_INPUT
				|| entity.get('entityType') === EntityTypes.DATETIME_INPUT) {
				item.promptPreWidth = entity.get('promptPreWidth');
				item.promptPreText = entity.get('promptPreText');
				item.promptPostWidth = entity.get('promptPostWidth');
				item.promptPostText = entity.get('promptPostText');
				item.dependencies = entity.get('dependencies');
			} 
			
			if (entity.get('entityType') === EntityTypes.TEXT_INPUT) {
				item.inputType = entity.get('inputType');
				item.validators = entity.get('validators');
			} else if (entity.get('entityType') === EntityTypes.SELECT_INPUT) {
				item.selectionOptions = entity.get('selectionOptions');
			} else if (entity.get('entityType') === EntityTypes.CHECKBOX_INPUT) {
				item.defaultChecked = entity.get('defaultChecked');
			} else if (entity.get('entityType') === EntityTypes.TEXTAREA_INPUT) {
				item.rows = entity.get('rows');
			} else if (entity.get('entityType') === EntityTypes.DATETIME_INPUT) {
				item.inputType = entity.get('inputType');
				item.validators = entity.get('validators');
				item.precisionOptions = entity.get('precisionOptions');
				item.timeZone = entity.get('timeZone');
				item.allDateComponentsRequired = entity.get('allDateComponentsRequired');
				item.displayTimeZoneOptions = entity.get('displayTimeZoneOptions');
			} else if (entity.get('entityType') === EntityTypes.TEXTBLOCK) {
				item.content = entity.get('content');
				item.backgroundColor = entity.get('backgroundColor');
				item.qxqTextBlock = entity.get('qxqTextBlock');
				item.qxqHeight = entity.get('qxqHeight');
			} else if (entity.get('entityType') === EntityTypes.IMAGEBLOCK) {
				item.title = entity.get('title');
				item.alt = entity.get('alt');
				item.imgUrl = entity.get('imgUrl');
				item.aspectRatio = entity.get('aspectRatio');
			} else if (entity.get('entityType') === EntityTypes.HIGHLIGHT_IMAGEBLOCK) {
				item.title = entity.get('title');
				item.alt = entity.get('alt');
				item.imgUrl = entity.get('imgUrl');
				item.aspectRatio = entity.get('aspectRatio');
				item.regions = entity.get('regions');
				item.hiddenRegions = entity.get('hiddenRegions');
			}

			return item;
		}, 

		canDrag: function(props, monitor) {
			return AppStateStore.getAppState().get('canDrag') && !props.appState.get('addingAssociations');
		},

		isDragging: function(props, monitor) {
			return props.entity.get('id') === monitor.getItem().id;
		},

		endDrag: function(props, monitor, component) {
			if (!monitor.didDrop()) {
				props.deleteFormEntity(props.entity.get('id'));
			}
		}
	};

	var formEntityDragSourceCollect = function(connect, monitor) {
		return {
			connectDragSource: connect.dragSource(),
			isDragging: monitor.isDragging(),
			connectDragPreview: connect.dragPreview()
		};
	};

	var formEntityDropTargetSpec = {
		canDrop: function(props, monitor) {
			return false;
		}
	};

	var formEntityDropTargetCollect = function(connect, monitor) {
		return {
			connectDropTarget: connect.dropTarget(),
			isOver: monitor.isOver({shallow: true})
		}
	};

	let DropTarget = ReactDnD.DropTarget;
	let DragSource = ReactDnD.DragSource;
	return DropTarget([DnDTypes.TEXT_BOX, DnDTypes.PROMPT], formEntityDropTargetSpec, formEntityDropTargetCollect)
		(DragSource(DnDTypes.TEXT_BOX, formEntityDragSourceSpec, formEntityDragSourceCollect)(FormEntity));
}

module.exports = {
	FormEntityHOC: FormEntityHOC
}