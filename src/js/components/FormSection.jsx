var React = require('react');
var ReactDOM = require('react-dom');
var ReactDnD = require('react-dnd');
var Immutable = require('immutable');
var _ = require('underscore');
var PureRenderMixin = require('react-addons-pure-render-mixin');
var getEmptyImage = require('react-dnd-html5-backend').getEmptyImage;

var FormDesignerActions = require('../actions/FormDesignerActions.js');
var AppDispatcher = require('../dispatcher/Dispatcher.js');
var ResizableBox = require('react-resizable').ResizableBox;

var FormEntityStore = require('../stores/FormEntityStore.js');
var AppStateStore = require('../stores/AppStateStore.jsx');
var HelperFunctions = require('../HelperFunctions.jsx');

var BlueprintWidths = require('../constants/blueprint-widths.js');
var Constants = require('../constants/Constants.js');
var EntityTypes = Constants.ENTITY_TYPES;
var DnDTypes = Constants.DND_TYPES;

var filler = new Immutable.Map({filler: true, height: 1, entityType: 'Filler'});

var FormSection = React.createClass({
	findAddedWidth: function(entityIndex, row, width, section) {
		return width - row.getIn([entityIndex, section]);
	},

	/**
	* Resizes a child Form Entity of this Form Section
	* @param {Number} id 
	* @param {Number} width
	* @return {String} section: width || promptPreWidth || promptPostWidth
	*/
	resizeFormEntity: function(id, width, section) {
		let childEntities = this.props.entity.get('childEntities');
		if (_.find(childEntities.toJS(), (childEntity) => childEntity.id === id)) { // Resized Form Entity is a direct child of this form section, not necessary?

			var row = this.getRow(id);
			var entityIndex = _.findIndex(row.toJS(), (formEntity) => formEntity.id === id);	
			var availableWidthInRow = this.findAvailableWidthInRow(id);
			var addedWidth = this.findAddedWidth(entityIndex, row, width, section) > availableWidthInRow 
				? availableWidthInRow : this.findAddedWidth(entityIndex, row, width, section);
			
			// Resize bigger
			if (addedWidth > 0) {
				if (row.get(entityIndex).get('last')) {
					row = row.updateIn([entityIndex, 'append'], append => append - addedWidth);
				} else {
					//id = formEntityPath.last()+1;
					var indexToEndOfRow = entityIndex + 1;
					var doneSetting = false;
					while (!row.getIn([indexToEndOfRow , 'last'])) {
						var newPrepend = row.getIn([indexToEndOfRow , 'prepend']) - addedWidth;
						if (newPrepend >= 0) {
							row = row.setIn([indexToEndOfRow , 'prepend'], newPrepend);
							doneSetting = true;
							break;
						} else {
							row = row.setIn([indexToEndOfRow , 'prepend'], 0);
							addedWidth = newPrepend * -1; // Leftover widths
						}
						indexToEndOfRow ++;
					}

					// Dealing the 'last' entity in row 
					if (!doneSetting) {
						var newPrepend = row.getIn([indexToEndOfRow, 'prepend']) - addedWidth;
						if (newPrepend >= 0) {
							row = row.setIn([indexToEndOfRow, 'prepend'], newPrepend);
						} else {
							row = row.setIn([indexToEndOfRow, 'prepend'], 0);
							row = row.updateIn([indexToEndOfRow, 'append'], append => append + newPrepend);
						}
					}	
				}
				if (row.getIn([entityIndex, 'entityType']) === EntityTypes.FORM_SECTION) {
					// Resize the appends / filler in Form Section after it's been resized
					row = row.updateIn([entityIndex, 'childEntities'], 
						function(childEntities) {
							childEntities = childEntities.map(
								function(entity) {
									if (entity.get('filler')) {
										entity = entity.update('width', width => width + addedWidth);
									} else if (entity.get('last')){
										entity = entity.update('append', append => append + addedWidth);
									}
									return entity;
								});
							return childEntities;
						}
					);
				}
				row = row.updateIn([entityIndex, section], width => width + addedWidth);
				//console.log(row.toJS())
			} else if (width < row.getIn([entityIndex, section])){
				// Resize Smaller
				/* Validate width for Form Sections
				 Form Sections cannot resize to a point where any point of a child Entity is not visible
				 The minimum width is the maximum value of prepend + width of a child Entity
				   Validate height with text in it 
				*/
				if (row.getIn([entityIndex, 'entityType']) === EntityTypes.FORM_SECTION) {
					var minWidth = row.getIn([entityIndex, 'childEntities'])
							.filter(entity => !entity.get('filler'))
							.reduce((previousWidths, currentEntity, index) => 
								{
									var extraWidth = (currentEntity.get('entityType') === EntityTypes.TEXT_INPUT
														|| currentEntity.get('entityType') === EntityTypes.TEXTAREA_INPUT
														|| currentEntity.get('entityType') === EntityTypes.SELECT_INPUT
														|| currentEntity.get('entityType') === EntityTypes.CHECKBOX_INPUT) ? 
										(currentEntity.get('promptPreWidth') + currentEntity.get('promptPostWidth')) : 0;
									previousWidths.maxWidth = 
										currentEntity.get('last') ? 
											(previousWidths.runningWidth + (currentEntity.get('width') + currentEntity.get('prepend') + extraWidth) > previousWidths.maxWidth 
												? previousWidths.runningWidth + (currentEntity.get('width') + currentEntity.get('prepend') + extraWidth)
												: previousWidths.maxWidth)
											: previousWidths.maxWidth;
									previousWidths.runningWidth =
										currentEntity.get('last') ?
											0 : previousWidths.runningWidth + (currentEntity.get('width') + currentEntity.get('prepend')) + extraWidth;
									return previousWidths;
								}, 
								{runningWidth: 0, maxWidth: 0}).maxWidth;
					if (width < minWidth) {
						width = minWidth;
						addedWidth =  minWidth - row.getIn([entityIndex, 'width']);
					}
					// Resize the appends / filler in Form Section after it's been resized
					row = row.updateIn([entityIndex, 'childEntities'], 
						function(childEntities) {
							childEntities = childEntities.map(
								function(entity) {
									if (entity.get('filler')) {
										entity = entity.set('width', width);
									} else if (entity.get('last')) {
										entity = entity.update('append', append => append + addedWidth);
									}
									return entity;
									});
							return childEntities;
						}
					);
				}

				var isValid = true;
				if (((section === 'promptPreWidth' && row.getIn([entityIndex, 'promptPreText']).length > 0 && width === 0)) 
					|| ((section === 'promptPostWidth' && row.getIn([entityIndex, 'promptPostText']).length > 0 && width === 0))) {
					isValid = false;
					addedWidth = 0;
				}
				

				var idToChange = row.getIn([entityIndex, 'last']) ? entityIndex : entityIndex + 1;
				var sideToChange = row.getIn([entityIndex, 'last']) ? 'append' : 'prepend';
				row = row.updateIn([idToChange, sideToChange], sideToChange => sideToChange - addedWidth);
				//isValid = isValid &&  window.setTimeout(this.isValidTextChange(), 500);
				row = row.setIn([entityIndex, section], isValid ? width : row.getIn([entityIndex, section]));
			}
			/*	Validate height for Form Sections
				The minimum height is the number of rows there are
				?When resized until minimum height, remove the filler?
			*/
			// if (row.getIn([entityIndex, 'entityType']) === 'FormSection') {
			// 	// Removing all trailing fillers
			// 	var childEntities = row.getIn([entityIndex, 'childEntities']);
			// 	while (childEntities.last() && childEntities.last().get('filler')) {
			// 		childEntities = childEntities.pop();
			// 	}
			// 	//var minHeight = this.getMinResizableHeight(childEntities);
			// 	var maxHeight = this.props.height;
			// 	minHeight /= 36;
			// 	if (height < minHeight) {
			// 		console.log('toosmall')
			// 		height = minHeight;
			// 		console.log('height gotta be ' + height)
			// 	} else if (height > maxHeight) {
			// 		height = maxHeight;
			// 	}
			// 	row = row.setIn([entityIndex, 'height'], height);
			// }

			FormDesignerActions.updateFormSection(this.props.entity.get('id'), this.mergeRow(row));
		}		
	},

	mergeRow: function(row) {
		let childEntities = this.props.entity.get('childEntities');
		let formEntityPathToRow = HelperFunctions.getFormEntityPathToId(row.get(0).get('id'), childEntities);
		let index = formEntityPathToRow.last();
		let rowIndex = 0;

		while (!childEntities.getIn(formEntityPathToRow.pop().push(index)).get('last')) {
			childEntities = childEntities.setIn(formEntityPathToRow.pop().push(index), row.get(rowIndex));
			index++;
			rowIndex++;
		}
		childEntities = childEntities.setIn(formEntityPathToRow.pop().push(index), row.get(rowIndex));
		return childEntities;
	},

	getMinResizableHeight: function(childEntities) {
		/*
			Minimum height is derived from DOM Elements
			because no explicit height defined in components

			Breaks the model of not having height defined, 
			however needed for boundaries during resizing
		*/
		var rows = ReactDOM.findDOMNode(this).children[0].children;
		var minHeight = 0;
		//console.log(rows)
		for (var i = 0; i < rows.length; i++) {
			if (rows[i].className.includes('row')) {
				console.log(rows[i])
				minHeight += rows[i].offsetHeight;
			}
		}
		return minHeight;
		// var rowsOfParent = ReactDOM.findDOMNode(this).children[0].children;
		// var height = 0;
		// console.log("wrapper" + this.props.id)
		// for (var i = 0; i < rowsOfParent.length; i++) {
		// 	if (rowsOfParent[i].className.includes('row')) {
		// 		var wrappersInRow = rowsOfParent[i].children;
		// 		for (var y = 0; y < wrappersInRow.length; y++) {
		// 			console.log(wrappersInRow[y].id)
		// 			if (wrappersInRow[y].id == "wrapper" + this.props.id) {
		// 				console.log('got em')
		// 				console.log(wrappersInRow[y])

		// 			}
		// 			//console.log(wrappersInRow[y])
		// 		}
		// 		//console.log('nextrownogga')

		// 	}
		// }
		// console.log(height)
		// 		console.log(ReactDOM.findDOMNode(this))

		// console.log(ReactDOM.findDOMNode(this).children[0].children[0].className)
		// var childEntities = childEntities.toJS();
		// var minResizableHeight = 0;
		// var rowHeight = 1;

		// for (var i = 0; i < childEntities.length; i++) {
		// 	var currentRowEntity = childEntities[i];
		// 	if (currentRowEntity.entityType === 'FormSection') {
		// 		rowHeight = rowHeight > childEntities[i].height ? rowHeight : childEntities[i].height;
		// 	}

		// 	if (currentRowEntity.last || currentRowEntity.filler) {
		// 		minResizableHeight += rowHeight;
		// 		rowHeight = 1;
		// 	}
		// }
		
		// return minResizableHeight;
	},

	findAvailableWidthInRow: function(id) {
		let formEntities = this.props.entity.get('childEntities').toJS();
		var index = _.findIndex(formEntities, (formEntity) => formEntity.id === id);

		if (formEntities[index].last) {
			return formEntities[index].append;
		}

		var availableWidth = 0;
		index++;
		while (!formEntities[index].last) {
			availableWidth += formEntities[index].prepend;
			index++;
		}
		availableWidth += formEntities[index].prepend + formEntities[index].append;
		return availableWidth;
	},

	getRow: function(id) {
		let childEntities = this.props.entity.get('childEntities').toJS();
		var row = [];

		for (var i = 0; i < childEntities.length; i++) {
			if (childEntities[i].last) {
				row.push(childEntities[i]);
				if (_.find(row, (childEntity) => childEntity.id === id)) {
					return Immutable.fromJS(row);
				}
				row = [];
			} else {
				row.push(childEntities[i]);
			}
		}
	},

	/* 
		Form Section resolves all the UI / cursor calculations & validations for Drag & Drop. 
		If the drop is valid, then call FormDesignerAction to update FormEntityStore

		Dropping from DragHome or another row will never have border issues, because consider:
			Case 1: Drop into prepend
				Given any prepend, there will no append on directly left of it (following the rules).	
			Case 2: Drop into append
				Any append will be from a 'last' Form Entity.

		Border Control ONLY when LEFT Form Entity is dropped onto either
			Case 1: Its own wrapper
			Case 2: Into the prepend of Form Entity directly right of it
	*/
	

	// Done before dropFormEntity is propogated up because calculating DOM height from immediate parents are easier
	hasEnoughHeight: function(id, height) {
		// also need to count for last filler? to count or not to count
		// also make last filler color coded? 
		var childEntities = this.props.childEntities;
		var rowIndex = 0;
		for (var i = 0; i < childEntities.length; i++) {
			if (childEntities[i].id === id) {
				break;
			}
			if (childEntities[i].last) {
				rowIndex++;
			}
		}		
		var rows = [].slice.call(ReactDOM.findDOMNode(this).children[0].children);
		var rowsBeforeInsertion = _.filter(rows.slice(0, rowIndex),
			function(element) {
				return element.className.includes('row');
			});
		var rowsAfterInsertion = _.filter(rows.slice(rowIndex+1),
			function(element) {
				return element.className.includes('row') && !element.children[0].className.includes('filler');
			});

		var sum = 0;
		var maxHeight = this.props.root ? ReactDOM.findDOMNode(this).offsetHeight : ReactDOM.findDOMNode(this).children[0].offsetHeight;
		// console.log((_.reduce(rowsBeforeInsertion, function(sum, element) {return sum + element.offsetHeight}, 0)));
		// console.log((_.reduce(rowsAfterInsertion, function(sum, element) {return sum + element.offsetHeight}, 0)));
		var availableHeight = maxHeight 
								- (_.reduce(rowsBeforeInsertion, function(sum, element) {return sum + element.offsetHeight}, 0)) 
								- (_.reduce(rowsAfterInsertion, function(sum, element) {return sum + element.offsetHeight	}, 0))

		return height <= availableHeight;
	},

	cleanUpTrailingFillers: function(childEntities, formEntityPath) {
		/* Not completed yet */
		var deleteIndex = childEntities.getIn(formEntityPath.pop()).size - 2; // -2 to get to the second to last row Index, since -1 will be the last filler
		while (deleteIndex >= 0 && childEntities.getIn(formEntityPath.pop().push(deleteIndex)).get('filler')) {
			childEntities = childEntities.updateIn(formEntityPath.pop(), list => list.delete(deleteIndex));
			deleteIndex--;
		}
		return childEntities;
	},	

	containsFormEntity: function(id, childEntities) {
		var formSections = [];
		for (var i = 0; i < childEntities.size; i++) {
			if (childEntities.get(i).get('id') === id) {
				return true;
			}
			if (childEntities.get(i).get('entityType') === 'FormSection') {
				formSections.push(childEntities.get(i).get('childEntities'));
			}
		}

		for (var i = 0; i < formSections.length; i++) {
			var found = this.containsFormEntity(id, formSections[i]);
			if (found) {
				return true;
			}
		}
		return false;
	},

	isValidDrop: function(droppedFormEntity, targetFormWrapper, dropIdX, targetComponent) {
		window.clearTimeout(this.timeout);
		let childEntities = this.props.entity.get('childEntities');

		var validDrop = false;
		var location;
		var additionalWidth = 0;
		var hoveredWrappers = [];

		var droppedFormEntityWidth = droppedFormEntity.width;
		if (droppedFormEntity.entityType === 'TextInput'
			|| droppedFormEntity.entityType === 'SelectInput'
			|| droppedFormEntity.entityType === 'CheckboxInput'
			|| droppedFormEntity.entityType === 'TextareaInput') {
			droppedFormEntityWidth += droppedFormEntity.promptPreWidth + droppedFormEntity.promptPostWidth;
		}

		if (droppedFormEntity.hasOwnProperty('files')) {
			droppedFormEntityWidth = 1;
		}

		if (droppedFormEntity.id === targetFormWrapper.id) {
			var prepends = Math.floor((dropIdX - targetComponent.left) / Constants.PREPEND_WIDTH);
			var appends = (targetFormWrapper.prepend + targetFormWrapper.append) - prepends;
			var formEntityPath = HelperFunctions.getFormEntityPathToId(targetFormWrapper.id, childEntities);
			location = 'self';
			if (prepends >= 0 && appends >= 0) {
				validDrop = true;
			} else if (appends < 0) {
				/* Border Control Case 1: It's own wrapper
					Checking Entity on RIGHT to see if it has prepend to 'absorb' the overflow 
				*/
				if (!childEntities.getIn(formEntityPath.push('last')) 
					&& childEntities.getIn(formEntityPath.pop().push(formEntityPath.last() + 1).push('prepend')) >= appends * -1) {
					validDrop = true;
				}
			}

			if (!childEntities.getIn(formEntityPath.push('last'))) {
				additionalWidth = childEntities.getIn(formEntityPath.pop().push(formEntityPath.last() + 1).push('prepend'));
				hoveredWrappers.push({
					id: childEntities.getIn(formEntityPath.pop().push(formEntityPath.last() + 1)).get('id'),
					location: 'prepend',
					collapse: true,
					hoveringEntity: droppedFormEntity
				});
			}

			if (targetFormWrapper.prepend === 0) {

			}

		} else if (targetFormWrapper.filler) {
			// NEED MORE CHECKS ON FILLER!
			// FILLER IS NO LONGER ALWAYS 24 WIDTH
			var prepends = Math.floor((dropIdX - targetComponent.left) / Constants.PREPEND_WIDTH);
			var appends = targetFormWrapper.width - (prepends + droppedFormEntityWidth);
			if (prepends >= 0 && appends >= 0) {
				validDrop = true;
			}
			location='filler';
		// Drop into a ('last' Form Entity) append
		} else if (dropIdX >= (targetComponent.right - targetFormWrapper.append * Constants.APPEND_WIDTH)) {
			var prepends = Math.floor((dropIdX - (targetComponent.right - targetFormWrapper.append * Constants.APPEND_WIDTH)) / Constants.PREPEND_WIDTH);
			var appends = targetFormWrapper.append - (prepends + droppedFormEntityWidth);
			location='append';
			if (prepends >= 0 && appends >= 0) {
				validDrop = true;
			}
		} else if (dropIdX < (targetComponent.left + targetFormWrapper.prepend * Constants.PREPEND_WIDTH)) {
			/* Border Control Case 2: Into the prepend of Form Entity directly right of it */
			location='prepend';
			let droppedFormEntityPath = HelperFunctions.getFormEntityPathToId(droppedFormEntity.id, childEntities);
			let targetFormWrapperPath = HelperFunctions.getFormEntityPathToId(targetFormWrapper.id, childEntities);
			const isAdjacent = droppedFormEntityPath.size === targetFormWrapperPath.size && droppedFormEntityPath.last() + 1 === targetFormWrapperPath.last();
			
			if (isAdjacent && !droppedFormEntity.last) {
				var prepends = droppedFormEntity.prepend + droppedFormEntity.append + droppedFormEntityWidth+ (Math.floor((dropIdX - targetComponent.left) / Constants.PREPEND_WIDTH)); // hanging off
				var appends = targetFormWrapper.prepend - ((Math.floor((dropIdX - targetComponent.left) / Constants.PREPEND_WIDTH)) + droppedFormEntityWidth);
				if (prepends >= 0 && appends >= 0) {
					validDrop = true;
				}
			} else {
				var prepends = Math.floor((dropIdX - targetComponent.left) / Constants.PREPEND_WIDTH);
				var appends = targetFormWrapper.prepend - (prepends + droppedFormEntityWidth);
				if (prepends >= 0 && appends >= 0) {
					validDrop = true;
				}
			}
		} else {
			/* Hovering over section below a FormEntity in the same row (Height of row = Form Section)
				Case when there is a Form Section in that row */
			location = 'bottom';
			validDrop = false;
		}

		if (droppedFormEntity.entityType === EntityTypes.PROMPT) {
			validDrop = false;
		}
		hoveredWrappers.push({
			id: targetFormWrapper.id, 
			location: location, 
			validDrop: validDrop, 
			additionalWidth: additionalWidth,
			hoveringEntity: droppedFormEntity});
		this.setState({hoveredWrappers: hoveredWrappers, hoveredFormInput: null});
		this.timeout = window.setTimeout(() => this.setState({hoveredWrappers: []}), 75);
	},

	dropPrompt: function(id, location) {
		if (this.isValidDropPrompt(id, location)) {
			let childEntities = this.props.entity.get('childEntities');
			let formEntityPath = HelperFunctions.getFormEntityPathToId(id, childEntities);
			childEntities = childEntities.setIn(formEntityPath.push(location + 'Width'), 1);

			let addedWidth = 1;

			if (childEntities.getIn(formEntityPath).get('last')) {
				childEntities = childEntities.updateIn(formEntityPath.push('append'), append => append - addedWidth);
			} else {
				let indexToEndOfRow = formEntityPath.last() + 1;
				let doneSetting = false;
				let newPrepend = 0;
				while (!childEntities.getIn(formEntityPath.pop().push(indexToEndOfRow).push('last'))) {
					newPrepend = childEntities.getIn(formEntityPath.pop().push(indexToEndOfRow).push('prepend')) - addedWidth;
					if (newPrepend >= 0) {
						childEntities = childEntities.setIn(formEntityPath.pop().push(indexToEndOfRow).push('prepend'), newPrepend);
						doneSetting = true;
						break;
					} else {
						childEntities = childEntities.setIn(formEntityPath.pop().push(indexToEndOfRow).push('prepend'), 0);
						addedWidth = newPrepend * -1;
					}
					indexToEndOfRow++;
				}

				if (!doneSetting) {
					if (newPrepend >= 0) {
						childEntities = childEntities.setIn(formEntityPath.pop().push(indexToEndOfRow).push('prepend'), newPrepend);
					} else {
						childEntities = childEntities.setIn(formEntityPath.pop().push(indexToEndOfRow).push('prepend'), 0);
						childEntities = childEntities.updateIn(formEntityPath.pop().push(indexToEndOfRow).push('append'), append + newPrepend);
					}
				}
			}
			FormDesignerActions.updateFormSection(this.props.entity.get('id'), childEntities);
		}
	},

	isValidDropPrompt: function(id, location) {
		let childEntities = this.props.entity.get('childEntities');
		var formEntityPath = HelperFunctions.getFormEntityPathToId(id, childEntities);
		var formInput = childEntities.getIn(formEntityPath);
		var availableWidthInRow = this.findAvailableWidthInRow(id);

		if (availableWidthInRow > 0 && formInput.get(location + 'Width') == 0) {
			// this.updateFormEntity(id, {[location + width]: 1});
			//this.setState({hoveredFormInput: {id: id, location: location, valid: true}});
			return true;
		} else {
			//this.setState({hoveredFormInput: {id: id, location: location, valid: false}});
			return false;
		}
	},

	timeout: null,

	getInitialState: function() {
		return {
			hoveredWrappers: []
		}
	},

	mixins: [PureRenderMixin],// can optimize further,  if root, check name

	componentWillReceiveProps: function(nextProps) {
 		 // this.setState({
    // 		hoveredWrappers: []
  		// });
	},

	// componentWillUpdate: function() {
	// 	console.log('updating')
	// },

	componentDidMount: function() {
    	this.props.connectDragPreview(getEmptyImage());
	},

	onResize: function(event, dimensions) {
		var newWidth = Math.ceil(dimensions.size.width / Constants.PIXEL_TO_WIDTH_CONSTANT);
		this.props.resizeFormEntity(this.props.entity.get('id'), newWidth, 'width');
		this.forceUpdate();	
	},

	onResizeStart: function(event) {
		FormDesignerActions.disableDrag();
	},

	onResizeStop: function(event) {
		FormDesignerActions.enableDrag();
	},

	// deprecated
	isValidTextChange: function(id) {
		// Text determines height. Height has be controlled, however, by the enclosing FormSection
		var rows = ReactDOM.findDOMNode(this).children[0].children;
		var height = 0;
		for (var i = 0; i < rows.length; i++) {
			if (rows[i].className.includes('row')) {
				height += rows[i].offsetHeight;
			}
		}

		var maxHeight = this.props.root ? ReactDOM.findDOMNode(this).offsetHeight : ReactDOM.findDOMNode(this).children[0].offsetHeight;
		console.log('max ' + maxHeight);
		console.log('im trying to be  : ' + height)


		return height <= maxHeight;
	},

	setSelectedEntity: function(e) {
		e.stopPropagation();
		FormDesignerActions.setSelectedEntity(this.props.entity.get('id'));
	},

	render: function() {
		var connectDragSource = this.props.connectDragSource;
		var connectDropTarget = this.props.connectDropTarget;
		var connectDragPreview = this.props.connectDragPreview;

		let entity = this.props.entity;
		let childEntities = entity.get('childEntities');
		
		//Separating entities into rows in order to render row DIVs
		var rows = [];
		var row = [];
		for (var i = 0; i < childEntities.size; i++) {
			row.push(childEntities.get(i));
			if (childEntities.get(i).get('last') || childEntities.get(i).get('entityType') === EntityTypes.FILLER) {
				rows.push(row.slice(0));
				row = [];	
			}
		}

		// Solves circular dependency
		var FormEntityWrapper = require('./FormEntityWrapper.jsx').FormEntityWrapper;
		if (entity.get('root')) {
			// Root FormSection is NOT resziable
			return connectDropTarget(
				<div
					className={" container root"}>
					<div>
						{rows.map((row, key) => 
							<div 
								className='row'
								key={key}> 
								{row.map((entity) => 
									<FormEntityWrapper
										entity={entity}
										key={entity.get('id')}

										dropFormEntity={this.props.dropFormEntity}
										dropPrompt={this.dropPrompt}
										deleteFormEntity={this.props.deleteFormEntity}
										resizeFormEntity={this.resizeFormEntity}
										isValidDrop={this.isValidDrop}
										isValidDropPrompt={this.isValidDropPrompt}

										hoveredWrapper={this.state.hoveredWrappers.find((hoveredWrapper) => hoveredWrapper.id === entity.get('id'))} // ?eventually put all hovered stuf in state
										appState={this.props.appState}/>
								)} 
							</div>)
						}
					</div>
				</div>
			);
		}

		var width = BlueprintWidths[entity.get('width')];
		var minConstraints = [Constants.MIN_ENTITY_WIDTH, Constants.MIN_ENTITY_HEIGHT];
		var draggableOpts = {grid: [Constants.ENTITY_GAP, Constants.ROW_HEIGHT]};
		var onResize = this.onResize;
		var onResizeStart = this.onResizeStart;
		var onResizeStop = this.onResizeStop;

		var opacity = this.props.isDragging ? 0 : 1;
		var style = {
			opacity: opacity,
			width: width,
			float: 'left',
			position: 'relative'
		};
		if (this.props.appState.get('selectedEntityId') === entity.get('id')) {
			style.background = 'green';
		}
		var className = 'span-' + entity.get('width') + ' formSection';
		let id = entity.get('id');

		return connectDropTarget(connectDragSource(
			<div 
				id={'formSection' + id}
				style={style}
				onClick={this.setSelectedEntity}>
				<ResizableBox
					width={width}
					className={className}

					minConstraints={minConstraints}
					draggableOpts={draggableOpts}
					onResize={onResize}
					onResizeStart={onResizeStart}
					onResizeStop={onResizeStop}>
					{rows.map((row, key) => 
						<div 
							className='row'
							key={key}> 
							{row.map((entity) => 
								<FormEntityWrapper
									entity={entity}
									key={entity.get('id')}

									dropFormEntity={this.props.dropFormEntity}
									dropPrompt={this.dropPrompt}
									deleteFormEntity={this.props.deleteFormEntity}
									resizeFormEntity={this.resizeFormEntity}
									isValidDrop={this.isValidDrop}
									isValidDropPrompt={this.isValidDropPrompt}

									hoveredWrapper={this.state.hoveredWrappers.find((hoveredWrapper) => hoveredWrapper.id === entity.get('id'))}
									appState={this.props.appState}/>
								)} 
							</div>)
						}
				</ResizableBox>
			</div>
		));
	}
});

var formSectionDragSourceSpec = {
	beginDrag: function(props) {
		const entity = props.entity;
		return {
			id: entity.get('id'),
			width: entity.get('width'),
			prepend: entity.get('prepend'),
			append: entity.get('append'),
			last: entity.get('last'),
			childEntities: entity.get('childEntities'),
			entityType: EntityTypes.FORM_SECTION
		};
	}, 

	canDrag: function(props, monitor) {
		return (!props.entity.get('root') && AppStateStore.getAppState().get('canDrag')) || props.appState.get('addingAssociation');
	},

	isDragging: function(props, monitor) {
		return props.entity.get('id') === monitor.getItem().id;
	},

	endDrag: function(props, monitor, component) {
		if (!monitor.didDrop()) {
			props.deleteFormEntity(props.entity.get('id'));
		}
	}
};

var formSectionDragSourceCollect = function(connect, monitor) {
	return {
		connectDragSource: connect.dragSource(),
		isDragging: monitor.isDragging(),
		connectDragPreview: connect.dragPreview()
	};
};

var formSectionDropTargetSpec = {
	canDrop: function(props, monitor) {
		return false;
	},
};

var formSectionDropTargetCollect = function(connect, monitor) {
	return {
		connectDropTarget: connect.dropTarget(),
		isOver: monitor.isOver()
	}
};

var DropTarget = ReactDnD.DropTarget;
var DragSource = ReactDnD.DragSource;

module.exports = {
	FormSection: DropTarget(DnDTypes.TEXT_BOX, formSectionDropTargetSpec, formSectionDropTargetCollect)
	(DragSource(DnDTypes.TEXT_BOX, formSectionDragSourceSpec, formSectionDragSourceCollect)(FormSection))
}