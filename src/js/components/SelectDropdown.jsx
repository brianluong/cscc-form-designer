var React = require('react');
var ReactDnD = require('react-dnd');
var ReactDOM = require('react-dom');
var $ = require('jquery');
var PureRenderMixin = require('react-addons-pure-render-mixin');
var Immutable = require('immutable');

var FormDesignerActions = require('../actions/FormDesignerActions.js');
var TextAreaAutoResize = require('./TextAreaAutoResize.jsx').TextAreaAutoResize;
var Portal = require('react-portal');

var Constants = require('../constants/Constants.js');

var SelectPortal = React.createClass({
	mixins: [PureRenderMixin],

	getInitialState: function() {
		return (
			{
				isOpen: false
			}
		);
	},

	openPortal: function(e) {
		const targetRect = e.target.getBoundingClientRect();
		this.setState(
			{
				isOpen: true,
				top: targetRect.top,
				left: targetRect.left
			}
		);
	},

	closePortal: function(e) {
		this.setState(
			{
				isOpen: false
			}
		);
	},

	render: function() {

		const editButtonStyle = {
			position: 'relative',
			top: '30%',
			width: '15px',
			height: '15px',
			float: 'left',
			cursor: 'pointer',
			backgroundSize: 'cover',
			backgroundImage: 'url(statics/editicon.png)'
		}
		
		return (
			<div
				style={{'height': '39px'}}
				>
				<div
					ref={'edit'}
					style={editButtonStyle}
					onClick={this.openPortal}>
				</div>
				<div
					style={{overflow: 'hidden'}}>
					<select
						className='select'>
						{this.props.selectionOptions.toJS().map((option, key) =>
							<option
								key={key}
								value={option.value}>
								{option.label}
							</option>
						)}
					</select>
				</div>
				<Portal
					isOpened={this.state.isOpen}
					closeOnOutsideClick
					onClose={() => this.setState({isOpen: false})}>
					<SelectDropdown 
						top={this.state.top} 
						left={this.state.left}
						id={this.props.id}
						selectionOptions={this.props.selectionOptions}
						closePortal={this.closePortal}/>
				</Portal>
			</div>
		);
	}
});

var SelectDropdown = React.createClass({

	getInitialState: function() {
		let selectionOptions = [];
		for (var i = 0; i < this.props.selectionOptions.size; i++) {
			selectionOptions.push(i);
		}
		return {selectionOptions: selectionOptions};
	},

	componentWillReceiveProps: function(nextProps) {
		if (nextProps.selectionOptions.size != this.props.selectionOptions.size) {
			let selectionOptions = [];
			for (var i = 0; i < nextProps.selectionOptions.size; i++) {
				selectionOptions.push(i);
			}
			this.setState({'selectionOptions': selectionOptions});
		}
	},

	addOption: function() {
		FormDesignerActions.updateFormEntity(this.props.id, 
			{
				'selectionOptions': this.props.selectionOptions.push(
					Immutable.fromJS({
						'label': 'Label ' + (this.props.selectionOptions.size + 1), 
						'value': 'Value ' + (this.props.selectionOptions.size + 1)
					}))
			});
	},

	deleteOption: function(index) {
		FormDesignerActions.updateFormEntity(this.props.id, 
			{
				'selectionOptions': this.props.selectionOptions.delete(index)
			});
	},

	updateOption: function(index, location, text) {
		FormDesignerActions.updateFormEntity(this.props.id, 
			{
				'selectionOptions': this.props.selectionOptions.setIn([index, location], text)
			});
	},

	reorderOptions: function(dragIndex, hoverIndex) {
		const draggedOption = this.props.selectionOptions.get(dragIndex);
		FormDesignerActions.updateFormEntity(this.props.id, 
			{
				'selectionOptions': this.props.selectionOptions.delete(dragIndex).insert(hoverIndex, draggedOption)
			});

		this.setState(function(previousState) {
			var prevSelectionOptions = previousState.selectionOptions.slice();
			const draggedOption = prevSelectionOptions[dragIndex];
			prevSelectionOptions.splice(dragIndex, 1);
			prevSelectionOptions.splice(hoverIndex, 0, draggedOption);
			return {'selectionOptions': prevSelectionOptions};
		});
	},

	render: function() {

		const styleDropdown = {
		 	'position': 'absolute',
			'border' : '1px solid gray',
			'top': this.props.top,
			'left': this.props.left,
			'background': 'white'
		};

		const styleAddButton = {
			'backgroundImage': 'url(statics/plus-button.jpeg)', 
			'backgroundSize': 'contain', 
			'backgroundRepeat': 'no-repeat', 
			'height': '15px',
		};

		const styleCloseButton = {
			'float': 'right'
		};

		return (
			<div
				style={styleDropdown}
				className={'span-7'}>
				<div
					className='close'/>
				{this.props.selectionOptions.toJS().map(
					(option, key) => 
						<SelectOptionDnD
							key={this.state.selectionOptions[key]}
							index={key}
							id={this.props.id}
							label={option.label}
							value={option.value}
							deleteOption={this.deleteOption}
							updateOption={this.updateOption}
							reorderOptions={this.reorderOptions}/>
				)}
				<div
					className={'span-7'}>
					<button 
						onClick={this.addOption}>
						Add Option
					</button>
					<button 
						style={styleCloseButton}
						onClick={this.props.closePortal}>
						Finish Edit
					</button>
				</div>
			</div>
		);

	}
});

var SelectOption = React.createClass({
	mixins: [PureRenderMixin],

	deleteOption: function() {
		this.props.deleteOption(this.props.index);
	},

	// Location =  {label || value}
	onTextChange: function(location, text) {
		this.props.updateOption(this.props.index, location, text);
	},

	render: function() {
		const {connectDragSource, connectDropTarget} = this.props;
		const styleOption = {
			'border': '1px solid gray',
			'position': 'relative',
			'overflowY': 'auto',
			'overflowX': 'hidden',
			'margin': '2px',
			'cursor': 'move',
			opacity: this.props.isDragging ? 0 : 1
		};

		const styleLabels = {
			'position': 'absolute', 
			'top': '50%', 
			'height': '20px', 
			'marginTop': '-10px'
		};

		const styleDeleteButton = {
			'backgroundImage': 'url(statics/x-button.png)', 
			'backgroundSize': 'contain', 
			'backgroundRepeat': 'no-repeat', 
			'height': '15px', 
			'width': '15px',
			'position': 'absolute', 
			'top': '50%', 
			'left': '240px', 
			'marginTop': '-7.5px'
		};

		return connectDragSource(connectDropTarget(
			<div
				style={styleOption}>
				<div
					className={'span-3'}>
					<div
						style={styleLabels}
						className={'span-1'}>
						Label:
					</div>
					<div
						style={{'height': '1px'}}
						className={'span-1'}/>
					<div
						className={'span-2 last'}>
						<TextAreaAutoResize
							id={this.props.id}
							type={'label'}
							text={this.props.label}
							onChange={this.onTextChange}/>
					</div>
				</div>
				<div
					className={'span-3 last'}>
					<div
						style={styleLabels}
						className={'span-1'}>
						Value:
					</div>
					<div
						style={{'height': '1px'}}
						className={'span-1'}/>
					<div
						className={'span-2 last'}>
						<TextAreaAutoResize
							id={this.props.id}
							type={'value'}
							text={this.props.value}
							onChange={this.onTextChange}/>
					</div>
				</div>
				<button
					style={styleDeleteButton}
					onClick={this.deleteOption}>
				</button>
			</div>
		));
	}
});

var selectOptionDragSourceSpec = {
	beginDrag: function(props, monitor, component) {
		return {
			index: props.index
		};
	}
};

var selectOptionDragSourceCollect = function (connect, monitor) {
	return {
		connectDragSource: connect.dragSource(),
		isDragging: monitor.isDragging()
	};
};

var selectOptionDropTargetSpec = {
	hover: function(props, monitor, component) {
		const dragIndex = monitor.getItem().index;
		const hoverIndex = props.index;

		if (dragIndex === hoverIndex) {
			return;
		}

	    // Determine rectangle on screen
	    const hoverBoundingRect = ReactDOM.findDOMNode(component).getBoundingClientRect();

	    // Get vertical middle
	    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

	    // Determine mouse position
	    const clientOffset = monitor.getClientOffset();

	    // Get pixels to the top
	    const hoverClientY = clientOffset.y - hoverBoundingRect.top;

	    // Only perform the move when the mouse has crossed half of the items height
	    // When dragging downwards, only move when the cursor is below 50%
	    // When dragging upwards, only move when the cursor is above 50%

	    // Dragging downwards
	    if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
	      return;
	    }

	    // Dragging upwards
	    if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
	      return;
	    }

	    props.reorderOptions(dragIndex, hoverIndex);
	    monitor.getItem().index = hoverIndex;	
	}
};

var selectOptionDropTargetCollect = function (connect, monitor) {
	return {
		connectDropTarget: connect.dropTarget()
	};
};


var DropTarget = ReactDnD.DropTarget;
var DragSource = ReactDnD.DragSource;
var SelectOptionDnD = DropTarget(Constants.SELECT_OPTION, selectOptionDropTargetSpec, selectOptionDropTargetCollect)
	(DragSource(Constants.SELECT_OPTION, selectOptionDragSourceSpec, selectOptionDragSourceCollect)(SelectOption));

module.exports = {
	SelectPortal: SelectPortal
}