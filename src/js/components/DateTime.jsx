const React = require('react');

const DateTime = React.createClass({
	render: function() {
			const calendarIconStyle = 
				{
					position: 'absolute',
					top: 0,
					right: 0,
					height: '15px',
					width:'15px',
					backgroundSize: 'cover',
					backgroundImage: 'url(statics/calendaricon.png)'
				}

		return (
			<div
				style={calendarIconStyle}/>
		);
	}
});

module.exports = {
	DateTime: DateTime
}