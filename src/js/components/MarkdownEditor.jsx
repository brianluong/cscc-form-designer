const React = require('react');
const Radium = require('radium');	
var ReactDnD = require('react-dnd');
const ToolTip = require('../../../../react-portal-tooltip');
const showdown = require('showdown');
const converter = new showdown.Converter();
const Portal = require('react-portal');
const ResizableBox = require('react-resizable').ResizableBox;

const FormDesignerActions = require('../actions/FormDesignerActions.js');

const Constants = require('../constants/Constants.js');
const MarkdownEditor = React.createClass({

	getInitialState: function() {
		return {
			width: 125,
			height: 125
		};
	},

	onResize: function(event, dimensions) {
		this.setState({
			width: dimensions.size.width,
			height: dimensions.size.height
		});
		this.forceUpdate();
	},

	render: function() {

		let editorStyle = {
			padding: '5px',
			top: this.props.position.top + 'px',
			left: this.props.position.left + 'px',
			position: 'absolute',
			background: 'beige',
			zIndex: 2 // If not, then editor may fall behind other HTML elements
		};

		let textAreaStyle = {
			width: '80%',
			height: '80%',
			resize: 'both'
		};

		return (
			<div
				style={editorStyle}
				onClick={(e) => {e.stopPropagation()}}
				onMouseDown={(e) => e.stopPropagation()}>
				
					<textarea
						style={textAreaStyle}
						value={this.props.text}
						onChange={this.props.onEditText}/>
				
			</div>
		);
	}
});

module.exports = {
	MarkdownEditor: MarkdownEditor
}