var React = require('react');
var ReactDnD = require('react-dnd');
var getEmptyImage = require('react-dnd-html5-backend').getEmptyImage;
var PureRenderMixin = require('react-addons-pure-render-mixin');
var Radium = require('radium');	

var FormEntityStore = require('../stores/FormEntityStore.js');
var AppStateStore = require('../stores/AppStateStore.jsx');

var FormDesignerActions = require('../actions/FormDesignerActions.js');
var FormEntity = require('./FormEntity.jsx').FormEntity;
var FormLayout = require('./FormLayout.jsx').FormLayout;
var Prompt = require('./Prompt.jsx').Prompt;

var BlueprintWidths = require('../constants/blueprint-widths.js');
var Constants = require('../constants/Constants.js');
var EntityTypes = Constants.ENTITY_TYPES;
var DnDTypes = Constants.DND_TYPES;

function FormInputFactory(formEntityType) {
	let FormInputTemplate = React.createClass({
		mixins: [PureRenderMixin],

		getInitialState: function() {
			return {hoveredFormEntity: null};
		},

		// All Form Inputs have Resizable Pre and Post Prompts
		// Must use this because onResizeXXXStop method doesn't return coordinates in grid increments
		// So will use the most recent grid coordinates saved in onResizeXXX method
		lastPromptWidth: 0,

		onResizePromptPre: function(event, dimensions) {
			var newWidth = Math.ceil(dimensions.size.width / Constants.PIXEL_TO_WIDTH_CONSTANT);
			this.lastPromptWidth = newWidth;
			this.props.resizeFormEntity(this.props.entity.get('id'), newWidth, 'promptPreWidth');
			this.forceUpdate(); // Force Update to also force update of TextAreaAutoResize
		},

		onResizePromptPost: function(event, dimensions) {
			var newWidth = Math.ceil(dimensions.size.width / Constants.PIXEL_TO_WIDTH_CONSTANT);
			this.lastPromptWidth = newWidth;
			this.props.resizeFormEntity(this.props.entity.get('id'), newWidth, 'promptPostWidth');
			this.forceUpdate();
		},

		onResizePromptPreStart: function(event) {
			FormDesignerActions.disableDrag();
			FormDesignerActions.setSelectedEntity(this.props.entity.get('id'));
			this.lastPromptWidth = this.props.entity.get('promptPreWidth');
		},

		onResizePromptPostStart: function(event) {
			FormDesignerActions.disableDrag();
			FormDesignerActions.setSelectedEntity(this.props.entity.get('id'));
			this.lastPromptWidth = this.props.entity.get('promptPreWidth');
		},

		onResizePromptPreStop: function(event, dimensions) {
			if (this.lastPromptWidth <= 0 && this.props.entity.get('promptPreText').length === 0) {
				FormDesignerActions.updateFormEntity(this.props.entity.get('id'), {promptPreWidth: 0, promptPreText: ''});
			}
			FormDesignerActions.enableDrag();
			this.forceUpdate();
		},

		onResizePromptPostStop: function(event, dimensions) {
			if (this.lastPromptWidth <= 0  && this.props.entity.get('promptPostText').length === 0) {
				FormDesignerActions.updateFormEntity(this.props.entity.get('id'), {promptPostWidth: 0, promptPostText: ''});
			}
			FormDesignerActions.enableDrag();
			this.forceUpdate();
		},

		// Hovering state for Pre and Post Prompts over the "Input"
		timer: null,

		onHoverFormEntity: function(side, isValid) {
			window.clearTimeout(this.timer);
			this.setState({hoveredFormEntity: {side: side, valid: isValid}});
			this.timer = window.setTimeout(() => this.setState({hoveredFormEntity: null}), 100);
		},

		render: function() {

			const entity = this.props.entity;
			let id = entity.get('id');
			let width = entity.get('width');
			let promptPreWidth = entity.get('promptPreWidth');
			let promptPreText = entity.get('promptPreText');
			let promptPostWidth = entity.get('promptPostWidth');
			let promptPostText = entity.get('promptPostText');
			
			return (
				<div>
					<Prompt
						id={id}
						type={'promptPre'}
						text={promptPreText}
						promptWidth={promptPreWidth}
						onResize={this.onResizePromptPre}
						onResizeStart={this.onResizePromptPreStart}
						onResizeStop={this.onResizePromptPreStop}/>

					<FormEntity
						entity={this.props.entity}
						onResize={this.props.onResize}
						onResizeStart={this.props.onResizeStart}
						onResizeStop={this.props.onResizeStop}
						dropPrompt={this.props.dropPrompt}
						isValidDropPrompt={this.props.isValidDropPrompt}
						onHover={this.onHoverFormEntity}
						hoveredProps={this.state.hoveredFormEntity}/>

					<Prompt
						id={id}
						type={'promptPost'}
						className={'end'}
						text={promptPostText}
						promptWidth={promptPostWidth}
						onResize={this.onResizePromptPost}
						onResizeStart={this.onResizePromptPostStart}
						onResizeStop={this.onResizePromptPostStop}/>
				</div>
			);
		}
	});

	return FormInputTemplate;	
}

module.exports = {
	FormInputFactory: FormInputFactory
}