const React = require('react');
const ReactDOM = require('react-dom');
const showdown = require('showdown');
const converter = new showdown.Converter();
const Portal = require('react-portal');


var FormEntityStore = require('../stores/FormEntityStore.js');
var AppStateStore = require('../stores/AppStateStore.jsx');

var FormDesignerActions = require('../actions/FormDesignerActions.js');
var AppDispatcher = require('../dispatcher/Dispatcher.js');
var FormEntity = require('./FormEntity.jsx').FormEntity;
var ResizableBox = require('react-resizable').ResizableBox;
var TextAreaAutoResize = require('./TextAreaAutoResize.jsx').TextAreaAutoResize;
var FormLayout = require('./FormLayout.jsx').FormLayout;
var TextAreaResizable = require('./TextAreaResizable.jsx').TextAreaResizable;
let MarkdownEditor = require('./MarkdownEditor.jsx').MarkdownEditor;



var BlueprintWidths = require('../constants/blueprint-widths.js');
var Constants = require('../constants/Constants.js');
var EntityTypes = Constants.ENTITY_TYPES;
var DnDTypes = Constants.DND_TYPES;

let FormEntityHOC = require('./FormEntityHOC.jsx').FormEntityHOC;


let TextBlock = React.createClass({

	getInitialState: function() {
		return {
			editorActive: false
		}
	},

	componentWillMount: function() {
		if (this.props.appState.get('selectedEntityId') !== this.props.entity.get('id')) {
			this.setState({editorActive: false});
		}
	},

	componentWillReceiveProps: function(nextProps) {
		if (nextProps.appState.get('selectedEntityId') !== this.props.entity.get('id')) {
			this.setState({editorActive: false});
		}

		if (nextProps.isDragging) {
			this.setState({editorActive: false});
		}
	},

	openMarkupEditor: function(e) {
		const targetRect = e.target.getBoundingClientRect();
		this.setState({
			editorActive: true,
			top: targetRect.top,
			left: targetRect.right
		})
	},

	onEditText: function(e) {
		FormDesignerActions.updateFormEntity(this.props.entity.get('id'), {content: e.target.value});
	},

	rawMarkup: function() {
		return {__html: converter.makeHtml(this.props.entity.get('content'))}
	},

	render: function() {

		const entity = this.props.entity;
		let id = entity.get('id');
		let width = entity.get('width');
		let entityType = entity.get('entityType');

		var maxConstraints = [Constants.MAX_ENTITY_WIDTH, Constants.ROW_HEIGHT];
		let content;
		let textEditor;
		if (entity.get('qxqTextBlock')) {
			content = <TextAreaResizable
						id={id}
						type={'qxqTextBlock'}
						width={width}
						rows={this.props.entity.get('qxqHeight')}
						onResize={(rows) => FormDesignerActions.updateFormEntity(id, {qxqHeight: rows})}
						rowHeight={35}
						backgroundColor={Constants.BACKGROUND_COLORS.VALUES[entity.get('backgroundColor')]}/>
		} else {
			let previewedContentStyle = {
				background: Constants.BACKGROUND_COLORS.VALUES[entity.get('backgroundColor')], 
				wordWrap: 'break-word',
				minHeight: '20px',
				marginTop: '5px',
				marginBottom: '12px',
				':hover': {
					cursor: ' pointer'
				}
			};

			content = <div
						id={'markdownPreview' + entity.get('id')}
						onClick={this.openMarkupEditor}
						key='keyForRadium1'>
							<div
								style={previewedContentStyle}
								dangerouslySetInnerHTML={this.rawMarkup()}/>
						</div>
		} 

		return (
			<div>
				<ResizableBox
					width={BlueprintWidths[width]}
					minConstraints={[Constants.MIN_ENTITY_WIDTH, Constants.MIN_ENTITY_HEIGHT]}
					maxConstraints={maxConstraints}
					draggableOpts={{grid: [Constants.ENTITY_GAP, 0]}}
					className={'textBlock ' + ' span-' + width}
					onResize={this.props.onResize}
					onResizeStart={this.props.onResizeStart}
					onResizeStop={this.props.onResizeStop}>
					{content}

					{!entity.get('qxqTextBlock') ? 
						<Portal
							isOpened={this.state.editorActive}>
							<MarkdownEditor
								text={this.props.entity.get('content')}
								position={{top: this.state.top, left: this.state.left}}
								onEditText={this.onEditText}/> 
						</Portal>
					: null}
				</ResizableBox>
			</div>
		);
	}
});

module.exports = {
	TextBlock: FormEntityHOC(TextBlock)
}
