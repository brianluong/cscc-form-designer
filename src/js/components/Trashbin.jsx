var React = require('react');
var ReactDnD = require('react-dnd');
var ReactDOM = require('react-dom');
var Constants = require('../constants/Constants.js');
var FormDesignerActions = require('../actions/FormDesignerActions.js');

var spec = {
	drop: function(props, monitor, component) {
		var item = monitor.getItem();
		FormDesignerActions.removeFormEntity(item.id);
	}
}

var collect = function(connect, monitor) {
	return {
		connectDropTarget: connect.dropTarget()
	}
};

var Trashbin = React.createClass({
	render: function() {
		var connectDropTarget = this.props.connectDropTarget;
		return connectDropTarget(
			<img src="trashbin.jpg" height="40" width="40">	
			</img>
		);
	}
})

var DropTarget = ReactDnD.DropTarget;
module.exports = {
	Trashbin: DropTarget(Constants.TEXT_BOX, spec, collect)(Trashbin)
}