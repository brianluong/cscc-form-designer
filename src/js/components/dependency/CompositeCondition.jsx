const React = require('react');
const Immutable = require('immutable');
const Collapse = require('rc-collapse');
const Panel = Collapse.Panel;

const FormDesignerActions = require('../../actions/FormDesignerActions.js');

const CompositeConditionPanel = require('./CompositeConditionPanel.jsx').CompositeConditionPanel;
const ValidatorCondition = require('./ValidatorCondition.jsx').ValidatorCondition;

const Constants = require('../../constants/Constants.js');
const OperatorTypes = Constants.OPERATOR_TYPES;


const CompositeCondition = React.createClass({
	getInitialState: function() {
		return {
			// Default open all CompositeCondition Panels
			activeKey: 'onlyPanel'
		}
	},

	toggleAccordion: function(activeKey) {
		this.setState({
			activeKey: activeKey
		});
	},

	selectOperator: function(e) {
		FormDesignerActions.updateFormEntityProperty(
			this.props.entityId, 
			this.props.keyPath.push('operator'), 
			e.target.value);
	},

	addValidator: function() {
		FormDesignerActions.updateFormEntityProperty(
			this.props.entityId, 
			this.props.keyPath.push('conditions'), 
			this.props.conditions.push(
				Immutable.fromJS(
					{
						dependentInputId: -1,
						properties: {
							name: '',
							value: ''
						},
						validState: true,
						strong: false,
						nullIsValid: false,
					})
			));
		this.setState({
			activeKey: 'onlyPanel'
		});
	},

	deleteValidator: function(validatorIndex) {
		return () => {
			FormDesignerActions.updateFormEntityProperty(
				this.props.entityId, 
				this.props.keyPath.push('conditions'), 
				this.props.conditions.delete(validatorIndex));
		}
	},

	addCondition: function() {
		FormDesignerActions.updateFormEntityProperty(
			this.props.entityId, 
			this.props.keyPath.push('conditions'), 
			this.props.conditions.push(
				Immutable.fromJS(
					{
						operator: OperatorTypes.AND,
						conditions: []
					})
			));
		this.setState({
			activeKey: 'onlyPanel'
		});
	},

	deleteCondition: function(validatorIndex) {
		return () => {
			FormDesignerActions.updateFormEntityProperty(
				this.props.entityId, 
				this.props.keyPath.push('conditions'), 
				this.props.conditions.delete(validatorIndex));
		}
	},

	renderConditions: function() {
		return this.props.conditions.map(
			(condition, index) => {
				if (condition.has('operator')) {
					return (
						<CompositeCondition
							entityId={this.props.entityId}
							keyPath={this.props.keyPath.concat(['conditions', index])}
							operator={condition.get('operator')}
							conditions={condition.get('conditions')}
							deleteCondition={this.deleteCondition(index)}/>
					);
				} else {
					return (
						<ValidatorCondition
							entityId={this.props.entityId}
							keyPath={this.props.keyPath.concat(['conditions', index])}
							validator={condition}
							deleteValidator={this.deleteValidator(index)}/>
					);
				}
			}
		).toJS();
	},

	render: function() {
		return (
			<Collapse
				activeKey={this.state.activeKey}
				onChange={this.toggleAccordion}>
				<Panel
					header={<CompositeConditionPanel
								operator={this.props.operator}
								selectOperator={this.selectOperator}
								addValidator={this.addValidator}
								addCondition={this.addCondition}
								deleteCondition={this.props.deleteCondition}
								root={this.props.root}/>}
					key='onlyPanel'>
					{this.renderConditions()}
				</Panel>
			</Collapse>
		);
	}
});




module.exports = {
	CompositeCondition: CompositeCondition
}