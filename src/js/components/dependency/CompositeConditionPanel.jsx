const React = require('react');

const Constants = require('../../constants/Constants.js');
const InputTypes = Constants.INPUT_TYPES;
const OperatorTypes = Constants.OPERATOR_TYPES;
const OperatorTypesDisplayNames = Constants.OPERATOR_TYPES_DISPLAY_NAMES;

const CompositeConditionPanel = React.createClass({


	render: function() {
		const compositeSelectorPanelStyle = {
			display: 'inline-block'
		};

		const deleteConditionButtonStyle = {
			backgroundImage: 'url(statics/x-button.png)', 
			backgroundSize: 'contain', 
			backgroundRepeat: 'no-repeat', 
			height: '15px', 
			width: '15px',
			verticalAlign: 'middle'
		};

		return (
			<div
				style={compositeSelectorPanelStyle}
				onClick={(e) => e.stopPropagation()}>
				<select
					value={this.props.operator}
					onChange={this.props.selectOperator}>
					{Object.keys(OperatorTypes).map(
						(operator) => 
							<option
								value={OperatorTypes[operator]}>
								{OperatorTypesDisplayNames[operator]}
							</option>
						)}
				</select>

				<span>
					<button
						onClick={this.props.addValidator}>
						+ Validator
					</button>
					<button
						onClick={this.props.addCondition}>
						+ Composite
					</button>
				</span>

				{!this.props.root ? 
					<button
						onClick={this.props.deleteCondition}
						style={deleteConditionButtonStyle}/>
				: null}
			</div>
		);
	}
})

module.exports = {
	CompositeConditionPanel: CompositeConditionPanel
}