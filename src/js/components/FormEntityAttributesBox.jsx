var React = require('react');
var ReactDOM = require('react-dom');
const Radium = require('radium');
let Select = require('react-select');
const _ = require('underscore');
let requireStyles = require('react-select/dist/react-select.css');

var FormDesignerActions = require('../actions/FormDesignerActions.js');

var HighlightRegions = require('./HighlightRegions.jsx').HighlightRegions;
let DateTimeAttributes = require('./DateTimeAttributes.jsx').DateTimeAttributes;
let ValidatorsBox = require('./validator/ValidatorsBox.jsx').ValidatorsBox;
let DependenciesBox = require('./dependency/DependenciesBox.jsx').DependenciesBox;
const Constants = require('../constants/Constants.js');
const InputTypes = Constants.INPUT_TYPES;
var EntityTypes = Constants.ENTITY_TYPES;
let BackgroundColors = Constants.BACKGROUND_COLORS;
var Immutable = require('immutable');



const FormEntityAttributesBox = Radium(React.createClass({
	getInitialState: function() {
		return {
			validatorsOpen: false,
			dependenciesOpen: false
		}
	},

	componentWillReceiveProps(nextProps) {
		if (nextProps.appState.get('selectedEntityId') !== this.props.appState.get('selectedEntityId')) {
			this.setState({
				validatorsOpen: false,
				dependenciesOpen: false
			});
		}
	},

	toggleValidators: function() {
		this.setState({
			validatorsOpen: !this.state.validatorsOpen
		});
	},

	toggleDependencies: function() {
		this.setState({
			dependenciesOpen: !this.state.dependenciesOpen
		});
	},

	render: function() {

		const entity = this.props.entity;
		if (!entity) {
			return null;
		}

		let entityType = entity.get('entityType');
		let id = entity.get('id');
		let width = entity.get('width');
		let promptPreWidth = entity.get('promptPreWidth');
		let promptPreText = entity.get('promptPreText');
		let promptPostWidth = entity.get('promptPostWidth');
		let promptPostText = entity.get('promptPostText');

		let attributes = null;
		if (entityType === EntityTypes.TEXT_INPUT
			|| entityType === EntityTypes.TEXTAREA_INPUT
			|| entityType === EntityTypes.SELECT_INPUT
			|| entityType === EntityTypes.CHECKBOX_INPUT
			|| entityType === EntityTypes.DATETIME_INPUT) {
				attributes = 
					(<div>
						<div>
							QxQ
							<textarea 
								rows={5}
								style={{width: '175px', height: 'auto'}}/>
						</div>

						<div>
							External Identifier:
							<input type='text'/>
						</div>

						<div>
							Name:
							<input type='text'/>
						</div>

						<div>
							SAS Code Label:
							<input type='text'/>
						</div>

						{entityType === EntityTypes.TEXT_INPUT 
							|| entityType === EntityTypes.DATETIME_INPUT ? 
							(<div>
								Enable Auto Tabbing
							<input type='checkbox'/>
							</div>) : null}
						
						{entityType === EntityTypes.TEXT_INPUT 
							|| entityType === EntityTypes.DATETIME_INPUT ? 
							(<div>
								<a 
									onClick={this.toggleValidators}
									style={{':hover': {cursor: 'pointer'}}}
									key='radiumUniqueKeyValidatorToggle'>
									Validators
								</a>
							</div>) : null}

						<div>
							<a
								onClick={this.toggleDependencies}
								style={{':hover': {cursor: 'pointer'}}}
								key='radiumUniqueKeyDependenciesToggle'>
								Dependencies
							</a>
						</div>

						{entityType === EntityTypes.TEXT_INPUT 
							|| entityType === EntityTypes.SELECT_INPUT ? 
							(<div>
								Input Type:
								<select
									onChange={(e) => FormDesignerActions.updateFormEntity(id, {inputType: e.target.value})}
									value={entity.get('inputType')}>
									{_.map(InputTypes, 
										(option, index) =>
											<option value={option}>
												{option.charAt(0).toUpperCase() + option.slice(1)}
											</option>)}
								</select>
								{entityType === EntityTypes.TEXT_INPUT ? 
									(<span>
										Double Key Entry
										<input type='checkbox'/>
									</span>) : null}
							</div>) : null}

						{entityType === EntityTypes.DATETIME_INPUT ?
							(<DateTimeAttributes
								entity={entity}/>) : null}
					</div>);
		} else if (entityType === EntityTypes.TEXTBLOCK) {
			attributes = (
				<div>
					<div>
						<Select
							options={[
								{label: BackgroundColors.NONE, value: BackgroundColors.NONE},
								{label: BackgroundColors.PINK, value: BackgroundColors.PINK},
								{label: BackgroundColors.PURPLE, value: BackgroundColors.PURPLE},
								{label: BackgroundColors.BLUE, value: BackgroundColors.BLUE},
								{label: BackgroundColors.GREEN, value: BackgroundColors.GREEN},
								{label: BackgroundColors.YELLOW, value: BackgroundColors.YELLOW},
								{label: BackgroundColors.GREY, value: BackgroundColors.GREY},
								{label: BackgroundColors.WHITE, value: BackgroundColors.WHITE}
							]}
							onChange={(value) => 
								FormDesignerActions.updateFormEntity(id, 
									{
										backgroundColor: value.value
									})}
							value={entity.get('backgroundColor')}/>
					</div>

					<div>
						<input 
							type='checkbox'
							checked={entity.get('qxqTextBlock')}
							onChange={(e) => {
								FormDesignerActions.updateFormEntity(id, {qxqTextBlock: e.target.checked});
							}}/>
						QxQ Text Block
					</div>
				</div>
			);
		} else if (entityType === EntityTypes.IMAGEBLOCK) {
			attributes = (	<div>
								Image Block
								<div>
									Title:
									<input type='text'/>
								</div>

								<div>
									Alt:
									<input type='text'/>
								</div>

								<button 
									onClick={()=> FormDesignerActions.updateFormEntity(id, 
										{
											entityType: EntityTypes.HIGHLIGHT_IMAGEBLOCK,
											regions: Immutable.fromJS([]),
											selectedRegion: -1,
											hiddenRegions: Immutable.fromJS([])
										})}
									disabled={entity.get('imgUrl') ? false : true}>
									Transform to Highlight Image Block
								</button>
							</div>);
		} else if (entityType === EntityTypes.HIGHLIGHT_IMAGEBLOCK) {
			attributes = (	<div>
								Highlight Image Block
								<div>
									Title:
									<input type='text'/>
								</div>

								<div>
									Alt:
									<input type='text'/>
								</div>

								<HighlightRegions
									id={id}
									imgUrl={entity.get('imgUrl')}
									color={entity.get('color')}
									aspectRatio={entity.get('aspectRatio')}
									width={width}
									regions={entity.get('regions')}
									hiddenRegions={entity.get('hiddenRegions')}
									appState={this.props.appState}/>
							</div>);
		}


		let uniqueAttributes = null;
		if (entityType === EntityTypes.TEXT_INPUT) {
			/* QxQ, Field Identifier, Field Name, Tab Order, Field Label, Enable Auto Tabbing
				Validators, Dependencies, Input Type -> Double Key Entry, Max Length */
			uniqueAttributes = (
				<div>
					Max Length
					<input 
						type='number'
						min="1" step="1" 
						defaultValue="1" 
						style={{width: '30px'}}/> 
				</div>);
		} else if (entityType === EntityTypes.TEXTAREA_INPUT) {
			/* QxQ, Field Identifier, Field Name, Tab Order, Field Label,
				Dependencies, Height */
			
		} else if (entityType === EntityTypes.CHECKBOX_INPUT) {
			/* QxQ, Field Identifier, Field Name, Tab Order, Field Label,
				Dependencies, Default State */
			
		} else if (entityType === EntityTypes.SELECT_INPUT) {
			/* QxQ, Field Identifier, Field Name, Tab Order, Field Label,
				Dependencies, Input Type*/
			
		}

		const formEntityAttributesStyle = {
			border: '1px grey solid',
			float: 'left',
			padding: '5px',
			width: '300px'
		};

		const addingAssociationsBlockStyle = {
			top: 0,
			left: 0,
			height: '100%',
			width: '100%'
		};

		return (
			<div
				style={{float: 'left'}}
				onClick={(e) => e.stopPropagation()}
				id={'formEntityAttributesBox'}>
					<div
						style={formEntityAttributesStyle}>
						{attributes}
						{uniqueAttributes}
						
						{this.props.appState.get('addingAssociations') ? 
							(<div
								style={addingAssociationsBlockStyle}/>
							) : null}
					</div>
					{this.state.validatorsOpen ? (
						<ValidatorsBox
							entity={this.props.entity}
							appState={this.props.appState}/>) 
					: null}
					{this.state.dependenciesOpen ? (
						<DependenciesBox
							entity={this.props.entity}
							appState={this.props.appState}
							entityId={this.props.entity.get('id')}
							dependencies={this.props.entity.get('dependencies')}/>) 
					: null}
			</div>
		);
	}
}));

module.exports = {
	FormEntityAttributesBox: FormEntityAttributesBox
}