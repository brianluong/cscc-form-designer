var React = require('react');

var ResizableBox = require('react-resizable').ResizableBox;
var TextAreaAutoResize = require('./TextAreaAutoResize.jsx').TextAreaAutoResize;

var BlueprintWidths = require('../constants/blueprint-widths.js');
var Constants = require('../constants/Constants.js');

let Prompt = React.createClass({
	render: function() {
		let zeroWidth = this.props.promptWidth <= 0;

		let promptStyle = {};
		if (this.props.type === 'promptPost') {
			promptStyle.marginRight = 0;
		}
		return (
			<div
				style={promptStyle}>
					<ResizableBox
						className={this.props.className + ' prompt' + ' span-' + this.props.promptWidth}
						width={BlueprintWidths[this.props.promptWidth]}
						onResize={this.props.onResize}
						onResizeStart={this.props.onResizeStart}
						onResizeStop={this.props.onResizeStop}
						minConstraints={[0, Constants.MIN_ENTITY_HEIGHT]}
						draggableOpts={{grid:[Constants.ENTITY_GAP, 0], handleSize:[zeroWidth ? 0 : 10, zeroWidth ? 0 : 10]}}>
							{!zeroWidth ? 
								<TextAreaAutoResize
									text={this.props.text}
									id={this.props.id}
									type={this.props.type}/> 
							: null}
					</ResizableBox>
			</div>);
	}
});

module.exports = {
	Prompt: Prompt
}